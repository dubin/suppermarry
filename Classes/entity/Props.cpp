#include "Props.h"




Props::Props()
{

}

Props::~Props()
{

}

Props* Props::createAndInit(const char *name, PROPS_TYPE_E type, const char *pszSpriteFrameName)
{
    Props *props = new Props();

    if (props == NULL)
    {
        return NULL;
    }

    if (props->initWithSpriteFrameName(pszSpriteFrameName) == false)
    {
        CC_SAFE_DELETE(props);
        return NULL;
    }

    if (props->extraInit() == false)
    {
        CC_SAFE_DELETE(props);
        return NULL;
    }

    props->setAnchorPoint(ccp(0,0));
    props->autorelease();
    props->setType(type);
    props->setName(string(name));
    props->setSpriteFrameName(string(pszSpriteFrameName));

    return props;
}

bool Props::extraInit()
{
    return true;
}

void Props::addObjectAt( CCPoint pos )
{
    this->setPosition(pos);

    this->setVisible(true);

    // 如果 某些 道具 是要加动画，则在此处加
    // this->setDisplayFrameWithAnimationName() . 然后zai update 里更新动画帧
    CCSpriteFrame *frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(
        this->getSpriteFrameName().c_str());

    this->setDisplayFrame(frame);


    this->unscheduleUpdate();
    this->scheduleUpdate();
}
