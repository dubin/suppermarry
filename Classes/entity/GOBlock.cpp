#include "GOBlock.h"
 

Block::Block()
{ 
}

Block::~Block()
{

}

Block* Block::createAndInit(const char *name, BLOCK_TYPE_E type, const char *pszSpriteFrameName)
{
    Block *block = new Block();

    if (block == NULL)
    {
        return NULL;
    }

    if (block->initWithSpriteFrameName(pszSpriteFrameName) == false)
    {
        CC_SAFE_DELETE(block);
        return NULL;
    }

    if (block->extraInit() == false)
    {
        CC_SAFE_DELETE(block);
        return NULL;
    }

    block->setAnchorPoint(ccp(0,0));
    block->autorelease();
    block->setType(type);
    block->setName(string(name));
    block->setSpriteFrameName(string(pszSpriteFrameName));

    return block;
}

bool Block::extraInit()
{ 
    return true;
} 
void Block::addObjectAt( CCPoint pos )
{
    this->setPosition(pos);

    this->setVisible(true);

    // 如果 某些 块 是要加动画，则在此处加
    // this->setDisplayFrameWithAnimationName() . 然后zai update 里更新动画帧
    CCSpriteFrame *frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(
            this->getSpriteFrameName().c_str());

    this->setDisplayFrame(frame);

    this->unscheduleUpdate();
    this->scheduleUpdate();
}
