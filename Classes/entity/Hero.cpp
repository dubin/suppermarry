#include "Hero.h"



Hero::Hero()
{
    m_name = "";
    m_spriteFrameName = "";

}

Hero::~Hero()
{

}

Hero* Hero::createAndInit(const char *name, HERO_TYPE_E type, const char *pszSpriteFrameName)
{
    Hero *hero = new Hero();

    if (hero == NULL)
    {
        return NULL;
    }
    
    if (hero->initWithSpriteFrameName(pszSpriteFrameName) == false)
    {
        CC_SAFE_DELETE(hero);
        return NULL;
    }

    if (hero->extraInit() == false)
    {
        CC_SAFE_DELETE(hero);
        return NULL;
    }

    

    hero->setAnchorPoint(ccp(0.5,0.5));
    hero->setScale(hero->getUserMultiScale());
    hero->autorelease();
    hero->setType(type);
    hero->setName(std::string(name));
    hero->setSpriteFrameName(std::string(pszSpriteFrameName));

    return hero;
}

float Hero::getUserMultiScale()
{
    return m_userScale;
}

bool Hero::extraInit()
{

    m_userScale = 1.0f;

    return true;
}

std::string Hero::getName()
{
    return m_name;
}

std::string Hero::getSpriteFrameName()
{
    return m_spriteFrameName;
}

HERO_TYPE_E Hero::getType()
{
    return m_type;
}

void Hero::setType( HERO_TYPE_E type )
{
    m_type = type;
}

void Hero::setName( std::string name )
{
    m_name = name;
}

void Hero::setSpriteFrameName(std::string frameName )
{
    m_spriteFrameName = frameName;
}

void Hero::setState(HERO_STATE_E state)
{
    m_state = state;
}

HERO_STATE_E Hero::getState()
{
    return m_state;
}