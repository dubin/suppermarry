
#include "GameObject.h"


GameObject::GameObject()
{
    m_name = "";
    m_spriteFrameName = "";
}

std::string GameObject::getName()
{
    return m_name;
}

std::string GameObject::getSpriteFrameName()
{
    return m_spriteFrameName;
}

int GameObject::getType()
{
    return m_type;
}

void GameObject::setType( int type )
{
    m_type = type;
}

void GameObject::setName( string  name )
{
    m_name = name;
}

void GameObject::setSpriteFrameName( std::string frameName )
{
    m_spriteFrameName = frameName;
}


