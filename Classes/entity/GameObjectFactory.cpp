#include "GameObjectFactory.h"

#include "GOBlock.h"
#include "Props.h"


CCSprite * GameObjectFactory::createObject( int type )
{
    CCSprite *sprite= NULL;

    if (type == BLOCK_TYPE_1)
    {
        sprite = (CCSprite*) Block::createAndInit("BLOCK_TYPE_1", BLOCK_TYPE_1, "block_1.png");
    }
    else if (type == BLOCK_TYPE_2)
    {
        sprite = (CCSprite*) Block::createAndInit("BLOCK_TYPE_2", BLOCK_TYPE_2, "block_2.png");
    }
    else if (type == BLOCK_TYPE_3)
    {
        sprite = (CCSprite*) Block::createAndInit("BLOCK_TYPE_3", BLOCK_TYPE_3, "block_3.png");
    }
    else if (type == BLOCK_TYPE_4)
    {
        sprite = (CCSprite*) Block::createAndInit("BLOCK_TYPE_4", BLOCK_TYPE_4, "block_4.png");
    }
    else if (type == PROPS_TYPE_1)
    {
        sprite = (CCSprite*) Props::createAndInit("PROPS_TYPE_1", PROPS_TYPE_1, "props_1.png");
    }

    return sprite;
}

std::string GameObjectFactory::getNameByType( int type )
{
    return string("");
}

std::string GameObjectFactory::getFrameNameByType( int type )
{
    if (type == BLOCK_TYPE_1)
    {
        return string("block_1.png");
    }
    else if (type == BLOCK_TYPE_2)
    {
        return string("block_2.png");
    }
    else if (type == BLOCK_TYPE_3)
    {
        return string("block_3.png");
    }
    else if (type == BLOCK_TYPE_4)
    {
        return string("block_4.png");
    }
    else if (type == PROPS_TYPE_1)
    {
        return string("props_1.png");
    }

    return string("");
}
