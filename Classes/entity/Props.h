

#ifndef __PROPS_HH
#define __PROPS_HH


#include <string>
#include "cocos2d.h"
#include "typedef.h"

#include "GameObject.h"


USING_NS_CC;
using namespace std;


class Props: public GameObject
{
private:

    Props();
public:
    virtual ~Props();

    static Props* createAndInit(const char * name, PROPS_TYPE_E type, const char *pszSpriteFrameName);
    bool extraInit();

    void addObjectAt( CCPoint pos);
};






#endif

