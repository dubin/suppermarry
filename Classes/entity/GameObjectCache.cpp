#include "GameObjectCache.h"
#include "database/GameDataSource.h"

GameObjectCache * GameObjectCache::create()
{
    GameObjectCache *cache = new GameObjectCache();
    if (cache == NULL)
        return NULL;

    if (cache->init() == false)
    {
        CC_SAFE_DELETE(cache);
        return NULL;
    }

    cache->autorelease();

    return cache;

}

bool GameObjectCache::init()
{
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage("game_elements.png");

    m_batchNode = CCSpriteBatchNode::createWithTexture(texture);

    if (m_batchNode == NULL)
    {
        return false;
    }

    m_batchNodeInUsedCache1 = CCArray::create();
    m_batchNodeInUsedCache1->retain();
    m_batchNodeInUsedCache2 = CCArray::create();
    m_batchNodeInUsedCache2->retain();

    m_batchNodeIDLE = CCArray::create();
    m_batchNodeIDLE->retain();

    int max = GameDataSource::shared()->getBatchNodeMaxNum();

    for (int i = 0; i < max; i ++)
    {
        CCSprite *sprite = GameObjectFactory::createObject(BLOCK_TYPE_1);
        sprite->setVisible(false);
         
        m_batchNode->addChild(sprite);

        m_batchNodeIDLE->addObject(sprite);
    }

    this->addChild(m_batchNode);

    m_currentCacheNum = 0;

    return true;
}

void GameObjectCache::addObjectAt( CCPoint pos, int type )
{
    CCArray *objs = m_batchNode->getChildren();

    CCObject *obj = m_batchNodeIDLE->lastObject();
    if (obj == NULL)
    {
        // TODO:
        CCLog("error FOR get Node");
        return;
    }

    GameObject *o = (GameObject *) obj;

    o->setType(type);
    o->setSpriteFrameName(GameObjectFactory::getFrameNameByType(type));
    o->addObjectAt(pos);

    caching(obj);

}

void GameObjectCache::rmObjectAt(cocos2d::CCPoint pos)
{
    CCArray *objs = m_batchNode->getChildren();
    for (unsigned int i = 0; i < objs->count(); i++) {
        CCSprite *sprite = dynamic_cast<CCSprite*>(objs->objectAtIndex(i));
        if (!sprite) continue;
        if (sprite->boundingBox().containsPoint(pos)) {
            sprite->setVisible(false);
            break;
        }
    }
}

void GameObjectCache::caching(CCObject *obj)
{
    unsigned int max = (unsigned int) GameDataSource::shared()->getBatchNodeMaxNum();

    m_batchNodeIDLE->removeObject(obj, false);

    if (m_currentCacheNum % 2 == 0)
    {
        m_batchNodeInUsedCache1->addObject(obj);
    }
    else
    {
        m_batchNodeInUsedCache2->addObject(obj);
    }

    if (m_currentCacheNum % 2 == 0)
    {
        if (m_batchNodeInUsedCache1->count() >= max / 2 - 1)
        {
            m_currentCacheNum ++;

            rmObj2(m_batchNodeInUsedCache2, m_batchNodeIDLE);

        }
    }
    else 
    {
        if (m_batchNodeInUsedCache2->count() >= max / 2 - 1)
        {
            m_currentCacheNum ++;

            rmObj2(m_batchNodeInUsedCache1, m_batchNodeIDLE);
        }
    }
}

void GameObjectCache::rmObj2(CCArray *from , CCArray *to)
{
    CCObject *obj = NULL;

    while(from->count() != 0)
    {
        obj = from->lastObject();
        from->removeObject(obj, false);

        GameObject *gameObj = (GameObject*)obj;
        gameObj->setVisible(false);
        to->addObject(gameObj);
    }
}