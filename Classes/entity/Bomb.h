

#ifndef __BOMB_HH
#define __BOMB_HH


#include <string>
#include "cocos2d.h"
#include "typedef.h"

#include "GameObject.h"

#include "scenes/GameLayerImp.h"

USING_NS_CC;
using namespace std;

class GameLayerImp;

class Bomb: public GameObject
{
private:

    Bomb();
    
public:
    virtual ~Bomb();

    static Bomb* createAndInit(BOMB_TYPE_E type, GameLayerImp *gameLayerImp);
    
    bool extraInit();

    void addObjectAt( CCPoint pos);
    void update(float fDelta);
    void onDieActionDone2(CCNode *target, void *data);
    void runDieAction();
     
private:
    char *getSpriteFrameNameByTypeAndIndex(int type, int index);
    void setGameLayer(GameLayerImp *gameLayerImp);

    float m_timeDelta;
    int m_displayFrameIndex;
    int m_displayFrameCount;

    GameLayerImp *gameLayerImp;
};






#endif

