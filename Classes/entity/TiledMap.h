
#ifndef __MAP_HH
#define __MAP_HH


#include <string>
#include "typedef.h"
#include <map>

#include "cocos2d.h"

USING_NS_CC;


#define MAP_TILED_WIDTH_DEFAULT 48
#define MAP_TILED_HEIGHT_DEFAULT 48

using namespace std;


// 描述一个关卡的布置。
class TiledMap
{

public:
    TiledMap();
    TiledMap(int tiledWidth, int tiledHeight);
    ~TiledMap();

    CCSize getTileSize();
     void setTiledMapTiledSize(int tiledWidth, int tiledHeight);

    CCPoint mapTileCoordToPixel(CCPoint tileCoord); // 给出地图Tiled坐标，返回像素坐标
    CCPoint PixelCoordToMapTiled(CCPoint pixelCoord); // 给出像素坐标， 返回MAP Tiled坐标

    bool insertTileByTiledCoord(CCPoint tiledCoord, int type); // 在指定的map 坐标处加入类型, 返回 true
                                                  // 如果该处已经有物品，返回false
    bool insertTileByPixelCoord(CCPoint pixelCoord, int type); // 在指定的像素坐标处加入类型, 返回 true
                                                                 // 如果该处已经有物品，返回false
     
    bool clearTiledTypeByTiledCoord(CCPoint tiledCoord); // 清除指定位置type.

    bool typeByTiledCoord(CCPoint tiledCoord, int *type); // 根据MAP 坐标返回该处的 type，如果改成没有任何东西，返回 false
    bool typeByPixelCoord(CCPoint pixelCoord, int *type); // 根据像素 坐标返回该处的 type，如果改成没有任何东西，返回false

    int typeByTiledCoord(CCPoint tiledCoord);
    int typeByPixelCoord(CCPoint pixelCoord);
    bool isBlock(int type);
    bool isProps(int type);
    bool isEmpty(int type);
private:
    void insert(int key, int type);
   
private:
    int m_tiledWidth;
    int m_tiledHeight;

    std::map<int, int> m_mapInfo; // key: tiledMAP 坐标X * MAP_KEY_MULT + Y. VALUE: TYPE

};



#endif