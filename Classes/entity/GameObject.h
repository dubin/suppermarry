

#ifndef __GAME_OBJECT_HH
#define __GAME_OBJECT_HH

#include "cocos2d.h"
#include <string>

USING_NS_CC;
using namespace std;


class GameObject: public CCSprite
{
public:
    GameObject();
   virtual ~GameObject(){}

    virtual void addObjectAt( CCPoint pos) = 0;

    int getType();
    void setType(int type);
    string getName();
    void setName(string  name);
    string getSpriteFrameName();
    void setSpriteFrameName(std::string  frameName);

private:
    int m_type;
    string m_name;
    string m_spriteFrameName;
};


#endif