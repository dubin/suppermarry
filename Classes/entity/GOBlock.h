
#ifndef __BLOCK_HH
#define __BLOCK_HH

#include <string>
#include "cocos2d.h"
#include "typedef.h"

#include "GameObject.h"

USING_NS_CC;
using namespace std;

class Block: public GameObject
{
private:

    Block();
public:
    ~Block();

    static Block* createAndInit(const char * name, BLOCK_TYPE_E type, const char *pszSpriteFrameName);
    bool extraInit();

    void addObjectAt( CCPoint pos);
};





#endif
