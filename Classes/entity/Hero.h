
#ifndef __HERO_HH
#define __HERO_HH

#include <string>
#include "cocos2d.h"
USING_NS_CC;
using namespace std;

typedef enum
{
    HERO_TYPE_1 = 1,
    HERO_TYPE_2,
}HERO_TYPE_E;

enum HERO_STATE_E {
    HERO_MOVING_STRAIGHT = 1,
    HERO_MOVING_JUMP_DOWN = 2,
    HERO_MOVEING_JUMP_UP = 3,
    HERO_MOVEING_UPDOWN = 4,
    HERO_MOVING_DOWN = 5,
    };

class Hero : public CCSprite
{
private:    
    Hero();
public:
    virtual ~Hero();

    static Hero* createAndInit(const char * name, HERO_TYPE_E type, const char *pszSpriteFrameName);
    bool extraInit();

    HERO_TYPE_E getType();
    void setType(HERO_TYPE_E type);
    string getName();
    void setName(std::string name);
    string getSpriteFrameName();
    void setSpriteFrameName(std::string frameName);

    void setState(HERO_STATE_E state);
    HERO_STATE_E getState();

    float Hero::getUserMultiScale();

private:
    HERO_TYPE_E m_type;
    std::string m_name;
    std::string m_spriteFrameName;
    HERO_STATE_E m_state;
    float m_userScale;
};



#endif