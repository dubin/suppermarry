#include "HeroFactory.h"



Hero * HeroFactory::createHero( HERO_TYPE_E type )
{
    Hero *hero = NULL;

    if (type == HERO_TYPE_1)
    {
        hero = Hero::createAndInit("TYPE_1", HERO_TYPE_1, "hero_1.png");
    }

    return hero;
}
