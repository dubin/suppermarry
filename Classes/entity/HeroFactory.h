#ifndef __HERO_FACTORY_HH
#define __HERO_FACTORY_HH

#include "cocos2d.h"

#include "Hero.h"


USING_NS_CC;


class HeroFactory
{
private:
    HeroFactory(){}
public:
    ~HeroFactory(){}

    static Hero * createHero(HERO_TYPE_E type);


};





#endif
