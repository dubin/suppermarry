#ifndef __GAME_OBJECT_FACTORY_HH
#define __GAME_OBJECT_FACTORY_HH

#include "cocos2d.h"

#include "typedef.h"

#include <string>


using namespace std;
USING_NS_CC;


class GameObjectFactory
{
private:
    GameObjectFactory(){}
public:
    ~GameObjectFactory(){}

    static CCSprite * createObject(int type);
    static string getNameByType(int type);
    static string getFrameNameByType(int type);
};





#endif
