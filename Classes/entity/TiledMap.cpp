
#include "TiledMap.h"

int formKey(int x, int y)
{
    return x * MAP_KEY_MULT + y;
}


TiledMap::TiledMap()
{
    this->m_tiledHeight = MAP_TILED_WIDTH_DEFAULT;
    this->m_tiledWidth  = MAP_TILED_HEIGHT_DEFAULT;
}

TiledMap::TiledMap(int tiledWidth, int tiledHeight)
{
    this->m_tiledHeight = tiledWidth;
    this->m_tiledWidth  = tiledHeight;
}

TiledMap::~TiledMap()
{

}

void TiledMap::setTiledMapTiledSize(int tiledWidth, int tiledHeight)
{
    this->m_tiledHeight = tiledWidth;
    this->m_tiledWidth  = tiledHeight;
}


cocos2d::CCSize TiledMap::getTileSize()
{
    return CCSize(m_tiledWidth, m_tiledHeight);
}

cocos2d::CCPoint TiledMap::mapTileCoordToPixel( CCPoint tileCoord )
{
    float x = tileCoord.x * m_tiledWidth;
    float y = tileCoord.y * m_tiledHeight;

    return CCPoint(x, y);
}

cocos2d::CCPoint TiledMap::PixelCoordToMapTiled( CCPoint pixelCoord )
{
    int col = ((int)pixelCoord.x) / m_tiledWidth;
    int row = ((int)pixelCoord.y) / m_tiledHeight;

    return CCPoint((float)col, (float)row);
}
 
bool TiledMap::insertTileByTiledCoord( CCPoint tiledCoord, int type )
{   
    int key = formKey((int)tiledCoord.x , (int)tiledCoord.y);
    int count = m_mapInfo.count(key);

    if (count == 1)
    {
        return false;
    }

    insert(key, type);

    return true;

}

bool TiledMap::insertTileByPixelCoord( CCPoint pixelCoord, int type )
{
    CCPoint tiledCoord = PixelCoordToMapTiled(pixelCoord);

    return insertTileByTiledCoord(tiledCoord, type);
}

bool TiledMap::typeByTiledCoord( CCPoint tiledCoord, int *type )
{
    map<int, int>::iterator iter;

    int key = formKey((int)tiledCoord.x , (int)tiledCoord.y);

    iter = m_mapInfo.find(key);

    if (iter == m_mapInfo.end())
    {
        return false;
    }

    int value = (int)iter->second;

    if (type != NULL)
    {
        *type = value;
    }

    return true;
}

bool TiledMap::typeByPixelCoord( CCPoint pixelCoord , int *type)
{
    CCPoint tiledCoord = PixelCoordToMapTiled(pixelCoord);

    return typeByTiledCoord(tiledCoord, type);
}


void TiledMap::insert( int key, int type )
{
    if (m_mapInfo.size() > MAP_TILED_ITEMS_MAX) // ���� �����
    {
        // deleted by db
        //m_mapInfo.clear();
    }

    m_mapInfo.insert(pair<int, int>(key, type));
}

bool TiledMap::clearTiledTypeByTiledCoord( CCPoint tiledCoord )
{
    map<int, int>::iterator iter;

    int key = formKey((int)tiledCoord.x , (int)tiledCoord.y);

    iter = m_mapInfo.find(key);

    if (iter != m_mapInfo.end())
    {
        m_mapInfo.erase(iter);
    }

    return true;
}


int TiledMap::typeByPixelCoord(cocos2d::CCPoint pixelCoord)
{
    CCPoint tiledPoint = PixelCoordToMapTiled(pixelCoord);
    return typeByTiledCoord(tiledPoint);
}

int TiledMap::typeByTiledCoord(cocos2d::CCPoint tiledCoord)
{
    int type;
    int key = formKey((int)tiledCoord.x, (int)tiledCoord.y);
    std::map<int, int>::iterator iter = m_mapInfo.find(key);
    if (iter == m_mapInfo.end())
        type = BP_TYPE_NULL;
    else
        type = (int)iter->second;
    return type;
}

bool TiledMap::isBlock(int type)
{
    return (type > BLOCK_TYPE_MIN && type < BLOCK_TYPE_MAX);
}

bool TiledMap::isProps(int type)
{
    return (type > PROPS_TYPE_MIN && type < PROPS_TYPE_MAX);
}

bool TiledMap::isEmpty(int type)
{
    return (type == BP_TYPE_NULL);
}