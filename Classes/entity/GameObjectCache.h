#ifndef __GAME_OBJECT_CACHE_HH
#define __GAME_OBJECT_CACHE_HH

#include "cocos2d.h"
#include "GameObjectFactory.h"
#include "GameObject.h"

USING_NS_CC;

class GameObjectCache: public CCNode
{
public:
    GameObjectCache(){}
    ~GameObjectCache(){}

    static GameObjectCache * create();
    bool init();

    void addObjectAt(CCPoint pos, int type);
    void rmObjectAt(CCPoint pos);
    void rmObj2(CCArray *from , CCArray *to);
    void caching(CCObject *obj);
private:
    CCSpriteBatchNode *m_batchNode;
    int m_nextInactiveBlock;

    CCArray *m_batchNodeInUsedCache1;
    CCArray *m_batchNodeInUsedCache2;
    int m_currentCacheNum;
    CCArray *m_batchNodeIDLE;

};


#endif
