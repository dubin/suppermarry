#include "Bomb.h"




Bomb::Bomb()
{

}

Bomb::~Bomb()
{

}

Bomb* Bomb::createAndInit(int type, GameLayerImp *gameLayerImp)
{
    Bomb *bomb = new Bomb();

    if (bomb == NULL)
    {
        return NULL;
    }

    if (bomb->initWithSpriteFrameName(bomb->getSpriteFrameNameByTypeAndIndex(type, 0)) == false)
    {
        CC_SAFE_DELETE(bomb);
        return NULL;
    }

    if (bomb->extraInit() == false)
    {
        CC_SAFE_DELETE(bomb);
        return NULL;
    }

    bomb->setAnchorPoint(ccp(0.5,0.5));
    bomb->autorelease();
    bomb->setType(type);
    bomb->setGameLayer(gameLayerImp);

    return bomb;
}

bool Bomb::extraInit()
{
    m_timeDelta = 0;
    m_displayFrameIndex = 0;
    m_displayFrameCount = 4;

    return true;
}

void Bomb::setGameLayer(GameLayerImp *gameLayerImp)
{
    this->gameLayerImp = gameLayerImp;
}

void Bomb::addObjectAt( CCPoint pos )
{
    this->setPosition(pos);
    this->setVisible(true);

    this->scheduleUpdate();
}

char *Bomb::getSpriteFrameNameByTypeAndIndex(int type, int index)
{
    char *frameNameBomb_1[] = {"bomb_1_0.png","bomb_1_1.png","bomb_1_2.png","bomb_1_3.png"};
    char *frameNameBomb_2[] = {"bomb_1_0.png","bomb_1_1.png","bomb_1_2.png","bomb_1_3.png"};
    char *frameNameBomb_3[] = {"bomb_1_0.png","bomb_1_1.png","bomb_1_2.png","bomb_1_3.png"};

    if (type >= BOMB_TYPE_MAX || type <= BOMB_TYPE_MIN)
    {
        return NULL;
    }

    if (index >= m_displayFrameCount)
        index = m_displayFrameCount - 1;
    if (index < 0)
        index = 0;

    switch(type)
    {
    case BOMB_TYPE_1:
        return frameNameBomb_1[index];
    case BOMB_TYPE_2:
        return frameNameBomb_2[index];
    default:
        return frameNameBomb_1[index];
    }

    return frameNameBomb_1[index];
}

void Bomb::update(float fDelta)
{
    m_timeDelta += fDelta;

    if (m_timeDelta < 0.8f)
    {
        return;
    }

    m_displayFrameIndex ++;
    m_timeDelta = 0;

    this->setScale(1.0f + m_displayFrameIndex *0.1f);

    if (m_displayFrameIndex >= m_displayFrameCount)
    {
        this->unscheduleUpdate();
        this->gameLayerImp->rmMagicObjectAtImpl(this->getPosition());
        runDieAction();
        
        return;
    }

    this->setDisplayFrame(
        CCSpriteFrameCache::sharedSpriteFrameCache()->
        spriteFrameByName(getSpriteFrameNameByTypeAndIndex(this->getType() , m_displayFrameIndex)));
}

void Bomb::runDieAction()
{
    CCScaleTo *s1 = CCScaleTo::create(0.2f, 0.5f);
    CCScaleTo *s2 = CCScaleTo::create(0.2f, 1.0f);

    CCSequence *seq1 = CCSequence::createWithTwoActions(s1, s2);

    this->runAction(seq1);

    CCScaleTo *s3 = CCScaleTo::create(0.2f, 2.5f);
    CCFadeOut *fo = CCFadeOut::create(0.2f);
    
    CCSequence *seq2 = CCSequence::createWithTwoActions(CCDelayTime::create(0.4f),s3);
    CCSequence *seq3 = CCSequence::create(CCDelayTime::create(0.4f),fo,
            CCCallFuncND::create(this, callfuncND_selector(Bomb::onDieActionDone2), (void*)NULL),
            NULL);
    this->runAction(seq2);
    this->runAction(seq3);
}

void Bomb::onDieActionDone2(CCNode *target, void *data)
{
    this->stopAllActions();
    this->removeFromParentAndCleanup(true);

}