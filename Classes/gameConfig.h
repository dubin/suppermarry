#ifndef __GAME_CONFIG_HH
#define __GAME_CONFIG_HH

#define __TARGET_IS_IPAD

// 定义按钮菜单的 位置
#ifdef __TARGET_IS_IPAD
#define MENU_TASK_MODE_POS_X (winSize.width / 2)
#define MENU_TASK_MODE_POS_Y  (100)

#define MENU_CHALL_MODE_POS_X (winSize.width / 2)
#define MENU_CHALL_MODE_POS_Y  (100)

#define MENU_SHOP_POS_X (winSize.width / 2)
#define MENU_SHOP_POS_Y  (100)

#define MENU_MY_ACHIEVEMENTS_POS_X (winSize.width)
#define MENU_MY_ACHIEVEMENTS_POS_Y  (winSize.height)

#define MENU_BACK_POS_X (winSize.width * 0.1f)
#define MENU_BACK_POS_Y (winSize.height * 0.9f)

#define MENU_START_GAME_POS_X (winSize.width / 2)
#define MENU_START_GAME_POS_Y (100)

#define MENU_PAUSE_GAME_POS_X (winSize.width * 0.9f)
#define MENU_PAUSE_GAME_POS_Y (winSize.height * 0.9f)

#define MENU_PAUSE_GAME_MENUS_POS_X (winSize.width / 2)
#define MENU_PAUSE_GAME_MENUS_POS_Y (winSize.height * 0.6f)


/// 解锁后获得的复活机会次数
#define MAX_LIFE_AFTER_UNLOCKED (10)


#endif


#endif