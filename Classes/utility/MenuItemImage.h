#pragma once
#include "cocos2d.h"

class MenuItemImage : public cocos2d::CCMenuItemImage
{
public:
	MenuItemImage(void);
	virtual ~MenuItemImage(void);

	static MenuItemImage* create(const char *normalImage, const char *selectedImage, CCObject* target, cocos2d::SEL_MenuHandler selector);
};

