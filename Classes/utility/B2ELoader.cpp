#include "B2ELoader.h"

unsigned char *getFileData(const char* pszFileName, const char* pszMode, unsigned long* pSize)
{
	unsigned char* pBuffer = NULL;
    *pSize = 0;
    do
    {
        // read the file from hardware
        FILE *fp = fopen(pszFileName, pszMode);
		assert(fp && "check you file");

        fseek(fp,0,SEEK_END);
        *pSize = ftell(fp);
        fseek(fp,0,SEEK_SET);
        pBuffer = new unsigned char[*pSize];
        *pSize = fread(pBuffer,sizeof(unsigned char), *pSize,fp);
        fclose(fp);
    } while (0);

	return pBuffer;
}

B2ELoader::B2ELoader(const char *jsonFile, double width, double height, double ptm)
{
	picojson::value v;
	std::string err;
	unsigned long len;
	unsigned char *data = getFileData(jsonFile, "r", &len);
	picojson::parse(v, data, data + len, &err);
	if (!err.empty()) {
		std::cerr << err << std::endl;
	} else {
		picojson::value bodies = v.get("rigidBodies");

		int size =bodies.get<picojson::array>().size();
		for (int i = 0; i < size; i++)
		{
			picojson::value obj = bodies.get(i);

			std::string bodyName = obj.get("name").get<std::string>();
			m_data[bodyName] = std::vector<Vertex *>();

            picojson::value origin = obj.get("origin");
            double originX = origin.get("x").get<double>();
            double originY = origin.get("y").get<double>();

            m_originPoint[bodyName] = b2Vec2(originX, originY);

			picojson::value polygons = obj.get("polygons");
			int psize = polygons.get<picojson::array>().size();

			for (int j = 0; j < psize; j++)
			{
				picojson::value polygon = polygons.get(j);
				int pointCnt = polygon.get<picojson::array>().size();
				
				Vertex *vertex = new Vertex();
				m_data[bodyName].push_back(vertex);

				b2Vec2 *vs = (b2Vec2 *) malloc (sizeof(b2Vec2) * pointCnt);
				memset(vs, 0, sizeof(b2Vec2) * pointCnt);
				vertex->vertex = vs;
				vertex->size = pointCnt;

				for (int k = 0; k < pointCnt; k++)
				{
					picojson::value point = polygon.get(k);
					double x = point.get("x").get<double>() - originX;
					x = (x * width) / ptm;
					double y = point.get("y").get<double>() - originY;
					y = (y * height) / ptm;
					vs[k].Set(x, y);
				}
			}
		}
	}
}


B2ELoader::~B2ELoader(void)
{
	// FIXME: memory leak
	std::map<std::string, std::vector<Vertex*> >::iterator it = m_data.begin();
	while (it != m_data.end()) {
		m_data.erase(it++);
	}
}

std::vector<Vertex *> & B2ELoader::vertex(const char *key)
{
	return m_data[std::string(key)];
}

b2Vec2 B2ELoader::originPoint(const char *key)
{
    return m_originPoint[std::string(key)];
}