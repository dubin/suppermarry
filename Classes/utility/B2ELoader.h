#pragma once

/**
 Physics Body Editor (previously known as box2d-editor)  https://code.google.com/p/box2d-editor/ Loader

 yurenjimi
 2013-03-26
 */

/*
{
"name1": [(polygon1,size), ...]
...
}
*/

#include <map>
#include <vector>

#include <Box2D/Box2D.h>
#include "picojson.h"

unsigned char *getFileData(const char* pszFileName, const char* pszMode, unsigned long* pSize);

typedef struct {
	b2Vec2 *vertex;
	int size;
} Vertex;

class B2ELoader
{
public:
	explicit B2ELoader(const char *jsonFile, double width, double height, double ptm);
	virtual ~B2ELoader(void);
	std::vector<Vertex*> & vertex(const char *key);
    b2Vec2 originPoint(const char *key);

private:
	std::map<std::string, std::vector<Vertex*> > m_data;
    std::map<std::string, b2Vec2 > m_originPoint;
};

