#include "MenuItemImage.h"

using namespace cocos2d;

MenuItemImage::MenuItemImage(void)
{
}


MenuItemImage::~MenuItemImage(void)
{
}

MenuItemImage* MenuItemImage::create(const char *normalImage
											, const char *selectedImage
											, CCObject* target
											, SEL_MenuHandler selector)
{
	CCSpriteFrame *normalSprite = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(normalImage);
	CCSpriteFrame *selectedSprite = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(selectedImage);

	return (MenuItemImage *) CCMenuItemSprite::create(CCSprite::createWithSpriteFrameName(normalImage)
		, CCSprite::createWithSpriteFrameName(selectedImage)
		, target
		, selector);
}