//
//  HeroAI.h
//  superMarry
//
//  Created by jryin on 13-5-6.
//
//

#ifndef __superMarry__HeroAI__
#define __superMarry__HeroAI__

#include "cocos2d.h"
USING_NS_CC;

class TiledMap;
class Hero;
class GameLayerImp;
class HeroAI : public cocos2d::CCNode
{
public:
    HeroAI();
    ~HeroAI();
    
    static HeroAI *createWithTiledMap(Hero *hero, TiledMap *tiledMap, GameLayerImp* layer);
    void update(float fDelta);

private:
    bool initWithTiledMap(Hero *hero, TiledMap *tiledMap, GameLayerImp* layer);
     
    void jumpStraight();
    void moveAI(float fDelta);
 
    void onJumpStraightDone(CCNode *target, void *data);
    void jumpUp(CCPoint from, CCPoint to);
    void onJumpUpDone(CCNode *target, void *data);
    void jumpDown(CCPoint from, CCPoint to);
    void onJumpDownDone(CCNode *target, void *data);
    CCPoint calculateNext();
    bool findBlockDown(int x, int fromY, int *whichY);
    void adujstHeroPosition();
    bool findBlockUp(int x, int fromY, int *whichY);
    void onUpDownDone(CCNode *target, void *data);
    void movingUpDown(CCPoint from, CCPoint to);
    bool heroEat();
    bool checkHeroIsOutofBound();
    bool adjustHeroInTiledMapBlocked();
    void movingDown(CCPoint from, CCPoint to);
    void onMovingDownDone(CCNode *target, void *data);
private:
    TiledMap *m_tiledMap;
    Hero     *m_hero;
};

#endif /* defined(__superMarry__HeroAI__) */
