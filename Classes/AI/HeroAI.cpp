//
//  HeroAI.cpp
//  superMarry
//
//  Created by jryin on 13-5-6.
//
//

#include "HeroAI.h"
#include "entity/TiledMap.h"
#include "database/GameDataSource.h"
#include "GameState.h"
#include "entity/Hero.h"
#include "scenes/GameLayerImp.h"
#include "scenes/SceneManager.h"

// 
float getJumpUpDownDelay(CCPoint from, CCPoint to);

HeroAI::HeroAI()
: m_tiledMap(NULL)
, m_hero(NULL)
{
    
}

HeroAI::~HeroAI()
{
    
}

HeroAI *HeroAI::createWithTiledMap(Hero *hero, TiledMap *tiledMap, GameLayerImp* layer)
{
    HeroAI *ai = new HeroAI();
    if (ai && ai->initWithTiledMap(hero, tiledMap, layer))
        ai->autorelease();
    else
        CC_SAFE_DELETE(ai);
    return ai;
}

bool HeroAI::initWithTiledMap(Hero *hero, TiledMap *tiledMap, GameLayerImp* layer)
{
    m_tiledMap = tiledMap;
    m_hero = hero;

    m_hero->setState(HERO_MOVING_STRAIGHT);

    m_hero->setPositionY(m_hero->getPositionY()-4);

    scheduleOnce(schedule_selector(HeroAI::moveAI), GameDataSource::shared()->getHeroMoveDuration());
    scheduleUpdate();

    return true;
}

void HeroAI::update(float fDelta)
{ 
    bool ret = false;

    heroEat();

    ret = checkHeroIsOutofBound();
    if (ret == true)
    {
        return;
    }

    ret = adjustHeroInTiledMapBlocked();
    if (ret == true)
    {
        return;
    }
}

bool HeroAI::heroEat()
{
    CCPoint pos = m_hero->getPosition();
    CCPoint tiledCoord = m_tiledMap->PixelCoordToMapTiled(pos);
    int curType = m_tiledMap->typeByTiledCoord(tiledCoord);
    
    if (m_tiledMap->isProps(curType))
    {
        GameLayerImp *parentLayer = (GameLayerImp *)this->getParent();
        parentLayer->rmObjectAt(tiledCoord.x, tiledCoord.y);
   
        GameDataSource::shared()->addTouchedStarNum(1);
    }

    return true;
}

bool HeroAI::checkHeroIsOutofBound()
{
    CCPoint pos = m_hero->getPosition();
 
    if (pos.y < GameDataSource::shared()->getBottomSideInPixel())
    { 
        m_hero->stopAllActions();
        this->unscheduleAllSelectors();
        SceneManager::shared()->loadGameOverScene();
        return true;
    }

    return false;
}

bool HeroAI::adjustHeroInTiledMapBlocked()
{
    CCPoint pos = m_hero->getPosition();
    CCPoint tiledCoord = m_tiledMap->PixelCoordToMapTiled(pos);
    CCPoint cPos = m_tiledMap->mapTileCoordToPixel(tiledCoord);

    int col = tiledCoord.x;
    int row = tiledCoord.y;

    int curType = m_tiledMap->typeByTiledCoord(tiledCoord);
    int down = m_tiledMap->typeByTiledCoord(ccp(col, row-1));
    int up = m_tiledMap->typeByTiledCoord(ccp(col, row+1));
    int left = m_tiledMap->typeByTiledCoord(ccp(col-1, row));
    int right = m_tiledMap->typeByTiledCoord(ccp(col+1, row));

    float nextPosX = cPos.x + GameDataSource::shared()->getTiledWidth() / 2;
    float nextPosY = cPos.y + m_hero->getContentSize().height / 2;

    if (m_tiledMap->isBlock(curType))
    {    
        if (m_tiledMap->isEmpty(right))
        {
            nextPosX += GameDataSource::shared()->getTiledWidth();
        }
        else if (m_tiledMap->isEmpty(up))
        {
            nextPosY += GameDataSource::shared()->getTiledHeight();
        }
        else if (m_tiledMap->isEmpty(left))
        {
            nextPosX -= GameDataSource::shared()->getTiledWidth();
        }
        else if (m_tiledMap->isEmpty(down))
        {
            nextPosY -= GameDataSource::shared()->getTiledWidth();
        }
        else
        {
            m_hero->stopAllActions();
            this->unscheduleAllSelectors();
            SceneManager::shared()->loadGameOverScene();
            return true;
        }

        goto _ADJUST_L;
    }
    else
    {
#if 0 // bug.
        if (m_tiledMap->isBlock(right) && m_tiledMap->isBlock(up) 
            && m_tiledMap->isBlock(down))
        {
            nextPosX -= GameDataSource::shared()->getTiledWidth();
            goto _ADJUST_L;
        }
#endif
    }

    return false;

_ADJUST_L: 
    unschedule(schedule_selector(HeroAI::moveAI));
    m_hero->stopAllActions();
    m_hero->setState(HERO_MOVING_STRAIGHT);
    m_hero->setPosition(ccp(nextPosX, nextPosY));
    scheduleOnce(schedule_selector(HeroAI::moveAI), GameDataSource::shared()->getHeroMoveDuration());

    return true;

}

void HeroAI::adujstHeroPosition()
{
    CCPoint pos = m_hero->getPosition();

    CCPoint tiledCoord = m_tiledMap->PixelCoordToMapTiled(pos);
    CCPoint newPos = m_tiledMap->mapTileCoordToPixel(tiledCoord);

    m_hero->setPositionX(newPos.x + GameDataSource::shared()->getTiledWidth() / 2);
    m_hero->setPositionY(newPos.y + m_hero->getContentSize().height / 2 - 4);
}

void HeroAI::moveAI(float fDelta)
{
    if (GameState::shared()->state() != kGameStateRunning)
    {
        return;
    }

    unschedule(schedule_selector(HeroAI::moveAI));

    adujstHeroPosition();

    CCPoint next = calculateNext();
    CCPoint posOffset = ccp(GameDataSource::shared()->getTiledWidth() / 2, 
        m_hero->getContentSize().height/2 * m_hero->getUserMultiScale());

    switch (m_hero->getState()) {
    case HERO_MOVING_STRAIGHT:
        jumpStraight();
        break;
    case  HERO_MOVING_JUMP_DOWN:
        jumpDown(m_hero->getPosition(), ccpAdd(next, posOffset));
        break;
    case HERO_MOVEING_JUMP_UP:
        jumpUp(m_hero->getPosition(), ccpAdd(next, posOffset));
        break;
    case HERO_MOVEING_UPDOWN:
        movingUpDown(m_hero->getPosition(), ccpAdd(next, posOffset));
        break;
    case HERO_MOVING_DOWN:
        movingDown(m_hero->getPosition(), ccpAdd(next, posOffset));
        break;
    default:
        break;
    }
}

CCPoint HeroAI::calculateNext()
{
    CCPoint pos = m_hero->getPosition();

    CCPoint tiledCoord = m_tiledMap->PixelCoordToMapTiled(pos);

    int curType = m_tiledMap->typeByTiledCoord(tiledCoord);
    int downType = m_tiledMap->typeByTiledCoord(ccp(tiledCoord.x, tiledCoord.y - 1));
    int rightType = m_tiledMap->typeByTiledCoord(ccp(tiledCoord.x + 1, tiledCoord.y));
    int upType = m_tiledMap->typeByTiledCoord(ccp(tiledCoord.x, tiledCoord.y + 1));
    int rightDownType = m_tiledMap->typeByTiledCoord(ccp(tiledCoord.x + 1, tiledCoord.y - 1));

     do{
            if (m_tiledMap->isEmpty(downType))
            {
                int y = tiledCoord.y - 2;
                int x = tiledCoord.x;
                int whichY = 0;
                bool found = false;

                if (tiledCoord.y <= 2)
                {
                    found = true;
                }
                else
                {
                    found = findBlockDown(x, y,&whichY);
                }

                if (found)
                {
                    m_hero->setState(HERO_MOVING_DOWN);
                    return m_tiledMap->mapTileCoordToPixel(ccp(x, whichY + 1));
                }
                else
                {
                    m_hero->setState(HERO_MOVING_STRAIGHT);
                    break;
                }
            }
            else if (m_tiledMap->isEmpty(rightType))
            {
                int y = tiledCoord.y - 1;
                int x = tiledCoord.x + 1;
                int whichY = 0;

                bool found = findBlockDown(x, y,&whichY);

                if (found && ( y == whichY))
                {
                    m_hero->setState(HERO_MOVING_STRAIGHT);
                    break;
                }
                else
                {
                    m_hero->setState(HERO_MOVING_JUMP_DOWN);
                    return m_tiledMap->mapTileCoordToPixel(ccp(x, whichY + 1));
                }
            }
            else if (m_tiledMap->isBlock(rightType))
            {
                int y = tiledCoord.y + 1;
                int x = tiledCoord.x + 1;
                int whichY = 0;
                int whichX = x;

                bool found = findBlockUp(x, y, &whichY);

                if (found)
                {
                    m_hero->setState(HERO_MOVEING_JUMP_UP);
                    return m_tiledMap->mapTileCoordToPixel(ccp(x, whichY));
                }
                else if (whichY != -1)
                {
                    m_hero->setState(HERO_MOVEING_UPDOWN);
                    return m_tiledMap->mapTileCoordToPixel(ccp(x - 1, whichY));
                }
            }
            else if (m_tiledMap->isProps(rightType))
            {
                m_hero->setState(HERO_MOVING_STRAIGHT);
            }

            break;
        }while(0);

    return ccp(0,0);
}

bool HeroAI::findBlockDown(int x, int fromY, int *whichY)
{

    int index = fromY;
    for (index = fromY; index > 0; index --)
    {
        int t = m_tiledMap->typeByTiledCoord(ccp(x, index));
        if (m_tiledMap->isBlock(t))
        {
            *whichY = index;
            return true;
        }
    }

    *whichY = 0;
    return false;
}

bool HeroAI::findBlockUp(int x, int fromY, int *whichY)
{
    int index = fromY;
    for (index = fromY; index <20; index ++)
    {
        int t1 = m_tiledMap->typeByTiledCoord(ccp(x, index));
        int t2 = m_tiledMap->typeByTiledCoord(ccp(x-1, index));

        if (m_tiledMap->isBlock(t2))
        { 
            *whichY = index;

            return false;
        }

        if (m_tiledMap->isEmpty(t1) || m_tiledMap->isProps(t1))
        {
            *whichY = index;
            return true;
        }
    }
     
    *whichY = -1;
    return false;
}

void  HeroAI::jumpStraight()
{
    int offset = GameDataSource::shared()->getTiledWidth();

    //
    float delay_stage1 = 0.1f;
    float delay_stage2 = 0.3f;
    float delay_stage3 = 0.1f;
    float delta_distance = 4.0f;
    float delta_scale = 0.85f;

    { // 处理反弹
        m_hero->runAction(CCSpawn::createWithTwoActions(
            CCMoveBy::create(delay_stage1,ccp(delta_distance,delta_distance)),
            CCScaleTo::create(delay_stage1, m_hero->getUserMultiScale())));
    }

    {
        CCJumpBy* jump = CCJumpBy::create(delay_stage2, ccp(offset-delta_distance * 2,0), offset / 3, 1);
        CCSequence *seq = CCSequence::create(CCDelayTime::create(delay_stage1), jump,
            NULL);
        m_hero->runAction(seq);
    }

    {
        CCMoveBy* move = CCMoveBy::create(delay_stage3,ccp(delta_distance,-delta_distance));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(CCDelayTime::create(delay_stage1 +delay_stage2 ), move);

        m_hero->runAction(seq);
    }

    {
        float us = m_hero->getUserMultiScale();
        CCScaleTo* scale = CCScaleTo::create(delay_stage3, us, us * delta_scale);
        CCSequence *seq = CCSequence::create(CCDelayTime::create(delay_stage1 +delay_stage2), scale,
             CCCallFuncND::create(this, callfuncND_selector(HeroAI::onJumpStraightDone), (void*)NULL),
             NULL);
        m_hero->runAction(seq);
    } 
}

void HeroAI::onJumpStraightDone(CCNode *target, void *data)
{
    m_hero->stopAllActions();

    scheduleOnce(schedule_selector(HeroAI::moveAI), GameDataSource::shared()->getHeroMoveDuration());
}


//////////////////////////////////////////////////////////////////////////
void HeroAI::movingUpDown(CCPoint from, CCPoint to)
{
    int offset = GameDataSource::shared()->getTiledWidth();

    //
    float delay_stage1 = 0.12f;
    float delay_stage2 = getJumpUpDownDelay(from, to);
    float delay_stage3 = 0.12f;
 
    float delay_stage4 = delay_stage2;
    float delay_stage5 = delay_stage3;
 

    float delta_distance = 4.0f;
    float delta_scale = 0.85f;

    { // 处理反弹
        m_hero->runAction(CCSpawn::createWithTwoActions(
            CCMoveBy::create(delay_stage1,ccp(0,delta_distance)),
            CCScaleTo::create(delay_stage1, m_hero->getUserMultiScale())));
    }

    { // 上升
        CCMoveBy* move = 
            CCMoveBy::create(delay_stage2,ccp(0, (to.y - from.y - delta_distance - m_hero->getContentSize().height)));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(CCDelayTime::create(delay_stage1), move);
        m_hero->runAction(seq);
    }

    {// 压缩 - 反弹
        CCMoveBy* move = CCMoveBy::create(delay_stage3,ccp(0, delta_distance));
        CCSequence *seq = 
            CCSequence::create(CCDelayTime::create(delay_stage1 +delay_stage2), 
            move, move->reverse(), NULL);

        m_hero->runAction(seq);
    }

    {// 缩小 -- 变大
        float us = m_hero->getUserMultiScale();
        CCScaleTo* scale1 = CCScaleTo::create(delay_stage3, us, us * delta_scale);
        CCScaleTo* scale2 = CCScaleTo::create(delay_stage3, us, us);

        CCSequence *seq = CCSequence::create(CCDelayTime::create(delay_stage1 +delay_stage2), 
            scale1, scale2, 
            NULL);
        m_hero->runAction(seq);
    }

    {// 下落
        CCMoveBy* move = CCMoveBy::create(delay_stage4,ccp(0, (from.y - to.y + delta_distance  + m_hero->getContentSize().height)));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(CCDelayTime::create(delay_stage1 +delay_stage2 + delay_stage3 * 2), move);
        m_hero->runAction(seq);
    }

    { // 压缩
        CCMoveBy* move = CCMoveBy::create(delay_stage5,ccp(0,  - delta_distance));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(
            CCDelayTime::create(delay_stage1 +delay_stage2 + delay_stage3 * 2 + delay_stage4 ), 
            move);

        m_hero->runAction(seq);
    }

    {// 缩小
        float us = m_hero->getUserMultiScale();
        CCScaleTo* scale = CCScaleTo::create(delay_stage5, us, us * delta_scale);
        CCSequence *seq = 
            CCSequence::create(CCDelayTime::create(delay_stage1 +delay_stage2 + delay_stage3 *2+ delay_stage4 ), scale,
            CCCallFuncND::create(this, callfuncND_selector(HeroAI::onUpDownDone), (void*)NULL),
            NULL);
        m_hero->runAction(seq);
    }
     
}

void HeroAI::onUpDownDone(CCNode *target, void *data)
{
    m_hero->stopAllActions();

    scheduleOnce(schedule_selector(HeroAI::moveAI), GameDataSource::shared()->getHeroMoveDuration());
}

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
void HeroAI::movingDown(CCPoint from, CCPoint to)
{
    int offset = GameDataSource::shared()->getTiledWidth();

    //
    float delay_stage1 = getJumpUpDownDelay(from, to);
    float delay_stage2 = 0.12f;

    float delta_distance = 4.0f;
    float delta_scale = 0.85f;

    {// 下落
        CCMoveBy* move = CCMoveBy::create(delay_stage1,ccp(0, (to.y - from.y)));
        m_hero->runAction(move);
    }

    { // 压缩
        CCMoveBy* move = CCMoveBy::create(delay_stage2,ccp(0,  - delta_distance));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(
            CCDelayTime::create(delay_stage1), 
            move);

        m_hero->runAction(seq);
    }

    {// 缩小
        float us = m_hero->getUserMultiScale();
        CCScaleTo* scale = CCScaleTo::create(delay_stage2, us, us * delta_scale);
        CCSequence *seq = 
            CCSequence::create(CCDelayTime::create(delay_stage1), scale,
            CCCallFuncND::create(this, callfuncND_selector(HeroAI::onMovingDownDone), (void*)NULL),
            NULL);
        m_hero->runAction(seq);
    }

}

void HeroAI::onMovingDownDone(CCNode *target, void *data)
{
    m_hero->stopAllActions();

    scheduleOnce(schedule_selector(HeroAI::moveAI), GameDataSource::shared()->getHeroMoveDuration());
}


// 

void HeroAI::jumpUp(CCPoint from, CCPoint to)
{
    int offset = GameDataSource::shared()->getTiledWidth();

    //
    float delay_stage1 = 0.1f;
    float delay_stage2 = getJumpUpDownDelay(from, to);
    float delay_stage3 = 0.3f;
    float delay_stage4 = 0.1f;
    float delta_distance = 4.0f;
    float delta_scale = 0.85f;

    { // 处理反弹
        m_hero->runAction(CCSpawn::createWithTwoActions(
            CCMoveBy::create(delay_stage1,ccp(0,delta_distance)),
            CCScaleTo::create(delay_stage1, m_hero->getUserMultiScale())));
    }

    {
        CCMoveBy* move = CCMoveBy::create(delay_stage2,ccp(0, (to.y - from.y - delta_distance)));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(CCDelayTime::create(delay_stage1), move);
        m_hero->runAction(seq);
    }

    {
        CCJumpBy* jump = CCJumpBy::create(delay_stage3, 
            ccp(offset-delta_distance, 0), 
            offset / 3, 1);
        CCSequence *seq = CCSequence::create(CCDelayTime::create(delay_stage1 + delay_stage2), jump,
            NULL);
        m_hero->runAction(seq);
    }

    {
        CCMoveBy* move = CCMoveBy::create(delay_stage4,ccp(delta_distance,-delta_distance));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(CCDelayTime::create(delay_stage1 +delay_stage2 + delay_stage3), move);

        m_hero->runAction(seq);
    }

    {
        float us = m_hero->getUserMultiScale();
        CCScaleTo* scale = CCScaleTo::create(delay_stage4, us, us * delta_scale);
        CCSequence *seq = CCSequence::create(CCDelayTime::create(delay_stage1 +delay_stage2 + delay_stage3), scale,
            CCCallFuncND::create(this, callfuncND_selector(HeroAI::onJumpUpDone), (void*)NULL),
            NULL);
        m_hero->runAction(seq);
    }
     
}

void HeroAI::onJumpUpDone(CCNode *target, void *data)
{
    m_hero->stopAllActions();
     
    scheduleOnce(schedule_selector(HeroAI::moveAI), GameDataSource::shared()->getHeroMoveDuration());
}
 
void HeroAI::jumpDown(CCPoint from, CCPoint to)
{
    int offset = GameDataSource::shared()->getTiledWidth();

    //
    float delay_stage1 = 0.1f;
    float delay_stage2 = 0.3f;
    float delay_stage3 = 0.1f;
    float delay_stage4 = getJumpUpDownDelay(from, to);

    float delta_distance = 4.0f;
    float delta_scale = 0.85f;

    { // 处理反弹
        m_hero->runAction(CCSpawn::createWithTwoActions(
            CCMoveBy::create(delay_stage1,ccp(delta_distance,delta_distance)),
            CCScaleTo::create(delay_stage1, m_hero->getUserMultiScale())));
    }

    {
        CCJumpBy* jump = CCJumpBy::create(delay_stage2, ccp(offset - delta_distance,0), offset / 3, 1);
        CCSequence *seq = CCSequence::create(CCDelayTime::create(delay_stage1), jump,
            NULL);
        m_hero->runAction(seq);
    }

    {
        CCMoveBy* move = CCMoveBy::create(delay_stage3,ccp(0, (to.y - from.y - delta_distance)));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(CCDelayTime::create(delay_stage1 +delay_stage2 ), move);

        m_hero->runAction(seq);
    }

    {
        CCMoveBy* move = CCMoveBy::create(delay_stage4,ccp(0,  - delta_distance));
        CCSequence *seq = 
            CCSequence::createWithTwoActions(CCDelayTime::create(delay_stage1 +delay_stage2 + delay_stage3 ), move);

        m_hero->runAction(seq);
    }

    {
        float us = m_hero->getUserMultiScale();
        CCScaleTo* scale = CCScaleTo::create(delay_stage3, us, us * delta_scale);
        CCSequence *seq = CCSequence::create(CCDelayTime::create(delay_stage1 +delay_stage2 + delay_stage3), scale,
            CCCallFuncND::create(this, callfuncND_selector(HeroAI::onJumpDownDone), (void*)NULL),
            NULL);
        m_hero->runAction(seq);
    }
     
}

void HeroAI::onJumpDownDone(CCNode *target, void *data)
{
    m_hero->stopAllActions();

    scheduleOnce(schedule_selector(HeroAI::moveAI), GameDataSource::shared()->getHeroMoveDuration());
}


//////////////////////////////////////////////////////////////////////////
float getJumpUpDownDelay(CCPoint from, CCPoint to)
{
    int tiledCount = fabs((from.y - to.y))/ GameDataSource::shared()->getTiledHeight();

    float arr[] = {0.3f, 0.3f, 0.4f,0.6f,0.8f,1.0f, 1.2f, 1.4f, 1.6f, 1.8f, 2.0f};

    int num = (sizeof(arr) / sizeof(arr[0]));

    if (tiledCount >= num)
        return arr[num - 1];
    return arr[tiledCount];

}





