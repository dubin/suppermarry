#ifndef __GALLERY_LAYER_H
#define __GALLERY_LAYER_H

#include "cocos2d.h" 
#include "cocos-ext.h"

#include <string>
using namespace std;

USING_NS_CC;
USING_NS_CC_EXT;

class GalleryView;
class GalleryViewCell;


class GalleryViewDelegate: public CCScrollViewDelegate
{
public:
    virtual void galleryCellTouched(GalleryView* table, GalleryViewCell* cell) = 0;
};

class GalleryViewCell:public CCNode
{
public:
    int getIdx(){return m_Id;}
    void setIdx(int uIdx) {m_Id = uIdx;}

private:
    int m_Id;
};

class GalleryViewPoint:public CCNode
{
public:
    int getIdx(){return m_Id;}
    void setIdx(int uIdx) {m_Id = uIdx;}

    void setFrameName(string normal, string selected) {m_normalFrameName = normal;  m_selectedFrameName = selected;}

    string getFrameNameNormal(){return m_normalFrameName;}
    string getFrameNameSelected(){return m_selectedFrameName;}

private:
    int m_Id;

    string m_normalFrameName;
    string m_selectedFrameName;
};

class GalleryView : public cocos2d::CCLayer ,public GalleryViewDelegate
{
public:
    virtual bool init();  

    virtual GalleryViewCell * makeViewCell(int index) = 0;
    virtual GalleryViewPoint * makeGalleryPointNav() = 0;

    // 子类要重载该三个实现
    virtual int numberOfCellsInGallery() {return 2;}
    virtual CCSize galleryViewVisiableSize() {return CCSizeMake(200,200);}
    virtual CCSize galleryContentSize() {return CCSizeMake(200,200);}

    virtual void galleryCellTouched(GalleryView* table, GalleryViewCell* cell) = 0;
 
    void setCurrentPage(int index) {m_nCurPage = index;}
    int getCurrentPage() {return m_nCurPage;}


     bool makeGalleryView();

public:
    //scrollview滚动的时候会调用
    void scrollViewDidScroll(CCScrollView* view);
    //scrollview缩放的时候会调用
    void scrollViewDidZoom(CCScrollView* view);

    virtual void onEnter();
    virtual void onExit();

    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

private:
    //根据手势滑动的距离和方向滚动图层
    void adjustScrollView(float offset);
    CCScrollView *m_pScrollView;

    GalleryViewPoint *m_viewPoint;

    CCPoint m_touchPoint;
    int m_nCurPage;

    bool m_isMoving;
    bool m_havePoints;
};

#endif