
#include "MyJewelryListViewLayer.h"

#include "utility/MenuItemImage.h"
#include "gameConfig.h"
#include "database/GameDataSource.h" 
#include "scenes/SceneManager.h"

USING_NS_CC;
USING_NS_CC_EXT;

MyJewelryListViewLayer::MyJewelryListViewLayer()
{

}

MyJewelryListViewLayer::~MyJewelryListViewLayer()
{

}

CCScene * MyJewelryListViewLayer::scene()
{
    CCScene *scene = CCScene::create();

    MyJewelryListViewLayer * layer = MyJewelryListViewLayer::create();
    scene->addChild(layer);
    return scene;
}

MyJewelryListViewLayer * MyJewelryListViewLayer::create()
{
    MyJewelryListViewLayer * layer = new MyJewelryListViewLayer();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool MyJewelryListViewLayer::init()
{ 
    bool ret = false;

    ret = GalleryView::init();
    if (ret == false)
    {
        return false;
    } 
    
    return true;
}

void MyJewelryListViewLayer::scrollViewDidScroll(CCScrollView *view)
{
}

void MyJewelryListViewLayer::scrollViewDidZoom(CCScrollView *view)
{
}

int MyJewelryListViewLayer::numberOfCellsInGallery()
{
    return 10;
}

cocos2d::CCSize MyJewelryListViewLayer::galleryViewVisiableSize()
{
    return CCSizeMake(200, 200);
}

cocos2d::CCSize MyJewelryListViewLayer::galleryContentSize()
{
    return CCSizeMake(200 * numberOfCellsInGallery(), 200);
}

void MyJewelryListViewLayer::galleryCellTouched( GalleryView* table, GalleryViewCell* cell )
{
    if (cell == NULL)
    {
        CCLog("Cell NUll");
    }
    else    {
        CCLog("gallery cell: %d", cell->getIdx());
    }
    
}

GalleryViewCell * MyJewelryListViewLayer::makeViewCell( int index )
{
    GalleryViewCell *pCell = new GalleryViewCell();
    pCell->autorelease();
    CCSprite *pSprite = CCSprite::createWithSpriteFrameName("jewelry_1.png");
    pSprite->setAnchorPoint(CCPointZero);
    pSprite->setPosition(ccp(0,30));
    pCell->addChild(pSprite);

    pCell = new GalleryViewCell();
    pCell->autorelease();
    pSprite = CCSprite::createWithSpriteFrameName("change_1.png");
    pSprite->setAnchorPoint(CCPointZero);
    pSprite->setPosition(CCPointZero);
    pCell->addChild(pSprite);

    pCell->setIdx(index);
    return pCell;
}

GalleryViewPoint * MyJewelryListViewLayer::makeGalleryPointNav()
{
    return NULL;
}
