#ifndef __MY_JEWELRY_LIST_VIEW_LAYER_HH
#define __MY_JEWELRY_LIST_VIEW_LAYER_HH

#include "cocos2d.h"
#include "cocos-ext.h"

#include "GalleryView.h"

USING_NS_CC;
USING_NS_CC_EXT;

class MyJewelryListViewLayer : public GalleryView
{
public:

    MyJewelryListViewLayer();
    ~MyJewelryListViewLayer();

    static cocos2d::CCScene * scene();
    static MyJewelryListViewLayer * create();
    bool init(); 
    // 
    virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view);

    virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);

    virtual GalleryViewCell * makeViewCell(int index);
    virtual GalleryViewPoint * makeGalleryPointNav();
    virtual int numberOfCellsInGallery();
    virtual CCSize galleryViewVisiableSize() ;
    virtual CCSize galleryContentSize();

    virtual void galleryCellTouched(GalleryView* table, GalleryViewCell* cell) ;


private:  
    void loadScrollView();
   
};



#endif
