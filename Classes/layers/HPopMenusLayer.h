#ifndef __HPOP_MENUS_LAYER_HH
#define __HPOP_MENUS_LAYER_HH

#include "cocos2d.h"
 
USING_NS_CC;

class HPopMenusLayer : public CCLayer
{
public:

    HPopMenusLayer();
    ~HPopMenusLayer();
     
    static HPopMenusLayer * create();
    bool init(); 
    
    void loadMenus();
    void loadFoldMenu();
    void loadUnFoldMenu();
    void onUnfoldMenuTouched(CCObject *sender);
    void onFoldMenuTouched(CCObject *sender);
    void onMusicMenuTouched(CCObject *sender);
    void onFoldActionDone(CCNode *target, void *data);
    void onUnFoldActionDone(CCNode *target, void *data);
private:
    CCNode *m_unfoldNode;
    CCNode *m_foldNode;

};



#endif
