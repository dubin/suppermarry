#include "GalleryView.h"
#include "autoscale/VisibleRect.h"

using namespace cocos2d;
using namespace cocos2d::extension;

bool GalleryView::init()
{
    bool bRet = false;
    do
    {
        CC_BREAK_IF( !CCLayer::init() );
 
        if (makeGalleryView() == false)
            break;
          
        bRet = true;
    }while(0);

    return bRet;
}

bool GalleryView::makeGalleryView()
{
    m_nCurPage = 0;
    m_havePoints = false;

    CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
    CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

    CCLayer *pLayer = CCLayer::create();
    int pageCount = numberOfCellsInGallery();

    for (int i = 0; i < pageCount; ++ i)
    {
        GalleryViewCell *cell = makeViewCell(i);

        cell->setPosition(ccp(visibleSize.width * i, 0));
        cell->setAnchorPoint(ccp(0,0));
        pLayer->addChild(cell,0, i);
    }

    m_pScrollView = CCScrollView::create(galleryViewVisiableSize(), pLayer);
    m_pScrollView->setContentOffset(CCPointZero);
    m_pScrollView->setTouchEnabled(false);
    m_pScrollView->setDelegate(this);
    m_pScrollView->setDirection(kCCScrollViewDirectionHorizontal);
    pLayer->setContentSize(galleryContentSize());

    this->addChild(m_pScrollView,0,0);

    if (m_havePoints)
    {
        m_viewPoint = makeGalleryPointNav();
        if (m_viewPoint != NULL)
            this->addChild(m_viewPoint);
    }

    return true;
}

void GalleryView::scrollViewDidScroll(cocos2d::extension::CCScrollView *view)
{
}

void GalleryView::scrollViewDidZoom(cocos2d::extension::CCScrollView *view)
{
}

void GalleryView::onEnter()
{
    CCLayer::onEnter();
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 1, false);
}

void GalleryView::onExit()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
    CCLayer::onExit();
}

bool GalleryView::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    m_touchPoint = pTouch->getLocation();

    m_isMoving = false;

    return true;
}

void GalleryView::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
    m_isMoving = true;
}

void GalleryView::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
    if (m_isMoving == false)
    {
        galleryCellTouched(this, (GalleryViewCell*)m_pScrollView->getContainer()->getChildByTag(m_nCurPage));
    }
    else
    {
        CCPoint endPoint = pTouch->getLocation();
        float distance = endPoint.x - m_touchPoint.x;
        if(fabs(distance) > 50)
        {
            adjustScrollView(distance);
        }
    }
}

void GalleryView::ccTouchCancelled(cocos2d::CCTouch *pTouch, cocos2d::CCEvent *pEvent)
{
    CCPoint endPoint = pTouch->getLocation();
    float distance = endPoint.x - m_touchPoint.x;
    if(fabs(distance) > 50)
    {
        adjustScrollView(distance);
    }
}

void GalleryView::adjustScrollView(float offset)
{
    CCSize visibleSize = VisibleRect::getVisibleRect().size;
    CCPoint origin = VisibleRect::leftBottom();

    CCSpriteFrameCache *pCache = CCSpriteFrameCache::sharedSpriteFrameCache();

    if (offset<0)
    {
        m_nCurPage ++;
    }else
    {
        m_nCurPage --;
    }

    if (m_nCurPage < 0)
    {
        m_nCurPage = 0;
    }

    if (m_nCurPage >= numberOfCellsInGallery())
    {
        m_nCurPage = numberOfCellsInGallery() - 1;
    }

    if(m_havePoints && m_viewPoint != NULL)
    {
        CCSprite *pPoint = (CCSprite *)m_viewPoint->getChildByTag(m_nCurPage);
        string name = m_viewPoint->getFrameNameNormal();
        CCSpriteFrame *sf = pCache->spriteFrameByName(name.c_str());

        pPoint->setDisplayFrame(sf);

        pPoint = (CCSprite *)m_viewPoint->getChildByTag(m_nCurPage);
        pPoint->setDisplayFrame(pCache->spriteFrameByName(m_viewPoint->getFrameNameSelected().c_str()));
    }   

    {
        CCPoint  adjustPos = ccp(origin.x - visibleSize.width * (m_nCurPage), 0);
        m_pScrollView->setContentOffsetInDuration(adjustPos, 0.3f);
    }
}