
#include "HPopMenusLayer.h"

#include "utility/MenuItemImage.h"
#include "gameConfig.h"
#include "database/GameDataSource.h" 
#include "autoscale/VisibleRect.h"

USING_NS_CC;


HPopMenusLayer::HPopMenusLayer()
{

}

HPopMenusLayer::~HPopMenusLayer()
{

}
 
HPopMenusLayer * HPopMenusLayer::create()
{
    HPopMenusLayer * layer = new HPopMenusLayer();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool HPopMenusLayer::init()
{  
    loadMenus();

    m_foldNode->setVisible(true);
    m_unfoldNode->setVisible(false);

    return true;
}

void HPopMenusLayer::loadMenus()
{
    loadFoldMenu();
    loadUnFoldMenu();
}

void HPopMenusLayer::loadFoldMenu()
{
    CCSize winSize = VisibleRect::getVisibleRect().size;

    CCArray *menus = CCArray::create();
    MenuItemImage *foldMenu = MenuItemImage::create("unfoldMenu.png"
        , "unfoldMenu.png"
        , this
        , menu_selector(HPopMenusLayer::onUnfoldMenuTouched));
    foldMenu->setPosition(0,0);
    menus->addObject(foldMenu);

    CCMenu *menu = CCMenu::createWithArray(menus);
    menu->setAnchorPoint(ccp(0,0));
    menu->setPosition(ccp(20,30));

    CCSprite *bkg = CCSprite::createWithSpriteFrameName("funcMenubkg1.png");
    bkg->setAnchorPoint(ccp(0,0));
    bkg->setPosition(ccp(0,0));

    m_foldNode = CCNode::create();
    m_foldNode->setAnchorPoint(ccp(0,0));
    m_foldNode->setPosition(0, 0);

    m_foldNode->addChild(bkg);
    m_foldNode->addChild(menu);
    
    this->addChild(m_foldNode);
}

void HPopMenusLayer::loadUnFoldMenu()
{
    CCSize winSize = VisibleRect::getVisibleRect().size;

    CCArray *menus = CCArray::create();
    MenuItemImage *unfoldMenu = MenuItemImage::create("foldMenu.png"
        , "foldMenu.png"
        , this
        , menu_selector(HPopMenusLayer::onFoldMenuTouched));
    unfoldMenu->setPosition(20,30);
    menus->addObject(unfoldMenu);

    MenuItemImage *musicMenu = MenuItemImage::create("musicMenu.png"
        , "musicMenu.png"
        , this
        , menu_selector(HPopMenusLayer::onMusicMenuTouched));
    musicMenu->setPosition(80,30);
    menus->addObject(musicMenu);

    CCMenu *menu = CCMenu::createWithArray(menus);
    menu->setAnchorPoint(ccp(0,0));
    menu->setPosition(ccp(0,0));

    CCSprite *bkg = CCSprite::createWithSpriteFrameName("funcMenubkg2.png");
    bkg->setAnchorPoint(ccp(0,0));
    bkg->setPosition(ccp(0,0));

    m_unfoldNode = CCNode::create();
    m_unfoldNode->setAnchorPoint(ccp(0,0));
    m_unfoldNode->setPosition(0, 0);

    m_unfoldNode->addChild(bkg);
    m_unfoldNode->addChild(menu);

    this->addChild(m_unfoldNode);
}


void HPopMenusLayer::onUnfoldMenuTouched(CCObject *sender)
{
    CCLog("Unfoled ");
     
    CCScaleTo *s1 = CCScaleTo::create(0.3f, 1.2f, 1.0f);
    CCScaleTo *s2 = CCScaleTo::create(0.2f, 0.01f,1.0f);
    CCSequence *seq = CCSequence::create(s1, s2, 
        CCCallFuncND::create(this, callfuncND_selector(HPopMenusLayer::onUnFoldActionDone), (void*)NULL),
        NULL);

    m_foldNode->runAction(seq);

}

void HPopMenusLayer::onUnFoldActionDone(CCNode *target, void *data)
{
    m_foldNode->setVisible(false);
    m_unfoldNode->setVisible(true);
    m_unfoldNode->setScaleX(0.01f);

    CCScaleTo *s1 = CCScaleTo::create(0.3f, 1.2f, 1.0f);
    CCScaleTo *s2 = CCScaleTo::create(0.2f, 1.0f,1.0f);
    CCSequence *seq = CCSequence::createWithTwoActions(s1, s2);

    m_unfoldNode->runAction(seq);
}

void HPopMenusLayer::onFoldMenuTouched(CCObject *sender)
{
    CCLog("Fold");

    CCScaleTo *s1 = CCScaleTo::create(0.3f, 1.2f, 1.0f);
    CCScaleTo *s2 = CCScaleTo::create(0.2f, 0.1f,1.0f);
    CCSequence *seq = CCSequence::create(s1, s2, 
        CCCallFuncND::create(this, callfuncND_selector(HPopMenusLayer::onFoldActionDone), (void*)NULL),
        NULL);

    m_unfoldNode->runAction(seq);
}

void HPopMenusLayer::onFoldActionDone(CCNode *target, void *data)
{
    m_foldNode->setVisible(true);
    m_unfoldNode->setVisible(false);

    m_foldNode->setScaleX(0.01f);

    CCScaleTo *s1 = CCScaleTo::create(0.3f, 1.2f, 1.0f);
    CCScaleTo *s2 = CCScaleTo::create(0.2f, 1.0f,1.0f);
    CCSequence *seq = CCSequence::createWithTwoActions(s1, s2);

    m_foldNode->runAction(seq);
}


void HPopMenusLayer::onMusicMenuTouched(CCObject *sender)
{
    CCLog("Music ON/OFF");
}
