
#ifndef __TYPEDEF_HH
#define __TYPEDEF_HH


#define kTagOverGameLayer 8
#define kTagPauseGameMenuItem 9
#define kTagGameSceneMenu     10
#define kTagGameScene   11

#define kZorderOverGameLayer 8


//////////////////////////////////////////////////////////////////////////
#define PTM_RATIO 32.0f

#define PI 3.141592

#define P2M(x) ((x)/PTM_RATIO)
#define M2P(x) ((x)*PTM_RATIO)

#define CCP2BVec(c) b2Vec2(P2M(c.x), P2M(c.y))
#define BVec2CCP(b) ccp(M2P(b.x), M2P(b.y))

#define EMIT(signal) if (signal) signal();

const float EP0  = 0.000000001f;
//////////////////////////////////////////////////////////////////////////

const int HERO_MOVE_SPEED = 4;
const int MAP_TILED_ITEMS_MAX = 512;

//////////////////////////////////////////////////////////////////////////
#define MAP_KEY_MULT 10000
const int BP_TYPE_NULL = 0;

const int BLOCK_TYPE_MIN = 0;
const int BLOCK_TYPE_1 = 1;
const int BLOCK_TYPE_2 = 2;
const int BLOCK_TYPE_3 = 3;
const int BLOCK_TYPE_4 = 4;
const int BLOCK_TYPE_MAX = 5;

const int PROPS_TYPE_MIN = 100;
const int PROPS_TYPE_1 = 101;
const int PROPS_TYPE_2 = 102;
const int PROPS_TYPE_3 = 103;
const int PROPS_TYPE_4 = 104;
const int PROPS_TYPE_MAX = 105;

const int BOMB_TYPE_MIN = 200;
const int BOMB_TYPE_1 = 201;
const int BOMB_TYPE_2 = 202;
const int BOMB_TYPE_3 = 203;
const int BOMB_TYPE_4 = 204;
const int BOMB_TYPE_MAX = 205;

typedef int BLOCK_TYPE_E;
typedef int PROPS_TYPE_E;
typedef int BOMB_TYPE_E;

#endif