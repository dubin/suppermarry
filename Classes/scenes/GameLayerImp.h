
#ifndef __GAME_LAYER_IMPL_HH
#define __GAME_LAYER_IMPL_HH


#include "cocos2d.h"
#include "GameScene.h"
#include "GameState.h"
#include "utility/MenuItemImage.h"
#include "entity/GameObjectCache.h"
#include "entity/HeroFactory.h"
#include "entity/Hero.h"
#include "entity/TiledMap.h"
#include "entity/Bomb.h"


USING_NS_CC;
 
class HeroAI;
class GameLayerImp : public CCLayer
{
public:

    GameLayerImp();
    ~GameLayerImp();

    static GameLayerImp * create();
    bool init();
    void update(float fDelta);

    void initHero();
    void initGameObjectCache();
    void initLevel();

    bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
    void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
    void registerWithTouchDispatcher();
    void tick(float fDelta);
    void onExit();
    void addObjectAt(int col, int row, int type);
    void addObjectAt(CCPoint loc, int type);
    CCPoint getLocationToLayer(CCPoint screenLoc);
    bool addObjectAtImpl(int col, int row, int type);
    void rmObjectAtImpl(int col, int row);
    void rmObjectAt(int col, int row);

    void rmMagicObjectAtImpl(CCPoint pos);
    void addMagicObj(int col, int row, int type);
    void onPropEatActionDone(CCNode *target, void *data);

    void runPropsDieAction(CCPoint pos, int type);
    void onBlockDieActionDone(CCNode *target, void *data);
    void runBlockDieAction(CCPoint pos, int type);
    
private:
    CCSize winSize;

    Hero * m_hero;
    GameObjectCache *m_gameObjectCache;
    TiledMap m_map;
    HeroAI *m_heroAi;

    // added by db 2013.5.7
    CCPoint m_heroLastPos;
    // ended
};





#endif