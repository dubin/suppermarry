
#include "GameScene.h"

#include "utility/MenuItemImage.h"

#include "gameConfig.h"
#include "SceneManager.h"
#include "database/GameDataSource.h"
#include "GameState.h"
#include "autoscale/VisibleRect.h"
#include "layers/HPopMenusLayer.h"

USING_NS_CC;

enum
{
kZorderGameBkg = -1,
kZorderGameLayer,
kZorderLine,
kZorderGameUI,
kZorderGameTop,
};

GameScene::GameScene()
{

}

GameScene::~GameScene()
{
}

CCScene * GameScene::scene()
{
    CCScene *scene = CCScene::create();

    GameScene * layer = GameScene::create();
    scene->addChild(layer);
    return scene;
}

GameScene * GameScene::create()
{
    GameScene * layer = new GameScene();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);

    return layer;
}

bool GameScene::init()
{
    this->setAnchorPoint(ccp(0,0));
    this->setPosition(0,0);
    this->setTag(kTagGameScene);

    loadGameScrollBackgroundLayer();
    loadGameLayer();
    
    loadMenus();

    loadHPOPMenus();

    loadInforBar();

    scheduleUpdate();

    GameState::shared()->startGame();

    return true;
}

void GameScene::loadGameScrollBackgroundLayer()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    m_scrollBackgroundLayer = GameScrollLayerImp::create();
    m_scrollBackgroundLayer->setAnchorPoint(ccp(0,0));
    m_scrollBackgroundLayer->setPosition(VisibleRect::leftBottom());
    this->addChild(m_scrollBackgroundLayer, kZorderGameBkg);
}

void GameScene::loadGameLayer()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    m_gameLayer = GameLayerImp::create();
    m_gameLayer->setAnchorPoint(ccp(0,0));
    m_gameLayer->setPosition(VisibleRect::leftBottom());
    this->addChild(m_gameLayer, kZorderGameLayer);
}

void GameScene::loadMenus()
{
  
}


void GameScene::loadInforBar()
{
    float labelPaddingY = 50.0f;
    float labelPaddingX = 150.0f;

    LevelData *ld = GameDataSource::shared()->getLevelData();
    unsigned int targetStarNum = ld->m_targetTouchedStarNum;

    {
        CCString *pString = NULL;
        pString = CCString::createWithFormat("Star: 0 / %d" , targetStarNum);

        m_targetStarInfoLabel = CCLabelTTF::create(pString->getCString(), "Helvetica", 28);

        m_targetStarInfoLabel->setAnchorPoint(ccp(0,0));
        m_targetStarInfoLabel->setPosition(ccpSub(VisibleRect::leftTop(), ccp(-labelPaddingX,labelPaddingY)));

        this->addChild(m_targetStarInfoLabel, kZorderGameTop);
    }
}

void GameScene::scrollingBackgroud(CCPoint delta)
{
    this->m_scrollBackgroundLayer->scrolling(delta);
}

void GameScene::loadHPOPMenus()
{
    HPopMenusLayer *hpMenu = HPopMenusLayer::create();

    hpMenu->setAnchorPoint(ccp(0,0));
    hpMenu->setPosition(VisibleRect::leftBottom().x, VisibleRect::leftBottom().y + 20);

    this->addChild(hpMenu, kZorderGameTop);
}

void GameScene::update(float fDelta)
{
    if (GameState::shared()->state() != kGameStateRunning)
    {
        unscheduleAllSelectors();
        return;
    }
     
    LevelData *ld = GameDataSource::shared()->getLevelData();
    unsigned int targetStarNum = ld->m_targetTouchedStarNum;
    unsigned int touchedStarNum = GameDataSource::shared()->getTouchedStarNum();
    
    CCString *pString = NULL;
    pString = CCString::createWithFormat("Star: %d / %d" ,touchedStarNum, targetStarNum);
    m_targetStarInfoLabel->setString(pString->getCString());

    if (touchedStarNum  >= targetStarNum)
    {
        unscheduleAllSelectors();
        SceneManager::shared()->loadGameOverScene();
        return;
    }


}

//////////////////////////////////////////////////////////////////////////
enum
{
    kZorderGameLayerBkg = -1,
    kZorderGameLayerLabel,
    kZorderGameLayerLine, 
};
GameScrollLayerImp::GameScrollLayerImp()
{

}

GameScrollLayerImp::~GameScrollLayerImp()
{
    
}

GameScrollLayerImp * GameScrollLayerImp::create()
{
    GameScrollLayerImp * layer = new GameScrollLayerImp();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool GameScrollLayerImp::init()
{ 
    m_scrollScale = 1.6f;

    loadBackgroud();
 
    initSpeedFactors();
 
    this->scheduleUpdate();

    return true;
}

void GameScrollLayerImp::initSpeedFactors()
{ 
    m_speedFactors.push_back(0.31f);
 

    CC_ASSERT(m_speedFactors.size() == m_numberSprites);
}

void GameScrollLayerImp::loadBackgroud()
{
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage("game_elements.png");

    m_spriteBatchNode = CCSpriteBatchNode::createWithTexture(texture);

    this->addChild(m_spriteBatchNode, kZorderGameLayerBkg);

    m_numberSprites = 1;

    { 
        CCSprite *sprite1 = CCSprite::createWithSpriteFrameName("gameBkg.png");
        sprite1->setAnchorPoint(ccp(0,0));
        sprite1->setScale(m_scrollScale);
        m_spriteBatchNode->addChild(sprite1, 0);

        sprite1 = CCSprite::createWithSpriteFrameName("gameBkg.png");
        sprite1->setAnchorPoint(ccp(0,0));
        sprite1->setPosition(ccp(VisibleRect::leftBottom().x + sprite1->getContentSize().width  * m_scrollScale - 1* m_scrollScale,
            VisibleRect::rightBottom().y));
        sprite1->setFlipX(true);
        sprite1->setScale(m_scrollScale);
        m_spriteBatchNode->addChild(sprite1, 0);

    }
}

void GameScrollLayerImp::update(float fDelta)
{
  //  scrolling(ccp(GameDataSource::shared()->getGamebackgroudScrollingSpreed() * fDelta,0));
}

void GameScrollLayerImp::scrolling(CCPoint delta)
{
    if (GameState::shared()->state() != kGameStateRunning)
    {
        return;
    }

    CCSprite *sprite;
    CCObject *obj;

    CCARRAY_FOREACH(m_spriteBatchNode->getChildren(), obj)
    {
        sprite = (CCSprite *)obj;
        int zOrder = sprite->getZOrder();

        if (zOrder != 0)
            continue;

        float factor = (m_speedFactors.at(zOrder));

        CCPoint pos = sprite->getPosition();

        pos.x += delta.x * factor;

        float left_max = - sprite->getContentSize().width - 10;
        float offset = (VisibleRect::getVisibleRect().size.width + sprite->getContentSize().width + 10 );

        if (zOrder == 0)
        {
            left_max = - sprite->getContentSize().width * m_scrollScale; // - VisibleRect::getVisibleRect().size.width;
            offset = sprite->getContentSize().width * 2 * m_scrollScale - 2 * m_scrollScale;
        }
         
        if (pos.x < left_max)
        {
            pos.x += offset;
        }

        sprite->setPosition(pos);
    }
}

