#ifndef __MAIN_SCENE_HH
#define __MAIN_SCENE_HH


#include "cocos2d.h"
#include "autoscale/VisibleRect.h"

USING_NS_CC;

class MainScene : public CCLayer
{
public:

    MainScene();
    ~MainScene();

    static cocos2d::CCScene * scene();
    static MainScene * create();
    bool init();

    void loadMenus();
    void onPlayGameMenuToched(CCObject *sender); 
    void loadHPOPMenus();

};



#endif
