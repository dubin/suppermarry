
#include "SplashScene.h"
#include "SceneManager.h"

USING_NS_CC;


SplashScene::SplashScene()
{

}

SplashScene::~SplashScene()
{

}

CCScene * SplashScene::scene()
{
    CCScene *scene = CCScene::create();

    SplashScene * layer = SplashScene::create();
    scene->addChild(layer);
    return scene;
}

SplashScene * SplashScene::create()
{
    SplashScene * layer = new SplashScene();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool SplashScene::init()
{
    createLabelStudioName();
    createLabelStudioNetAddr();

    loadResourcesAndFadeOut();

    return true;
}

void SplashScene::createLabelStudioName()
{
    m_labelStudioName = CCLabelTTF::create("A Bit Creative Studio Product", "Helvetica", 32);
 
    m_labelStudioName->setPosition(ccp(- m_labelStudioName->getContentSize().width, VisibleRect::center().y));

    this->addChild(m_labelStudioName);

    CCMoveTo *moveTo = CCMoveTo::create(1.5f, ccp(VisibleRect::center().x , VisibleRect::center().y));
    CCEaseExponentialInOut *easeIn = CCEaseExponentialInOut::create(moveTo);
    m_labelStudioName->runAction(easeIn);
}

void SplashScene::createLabelStudioNetAddr()
{ 
    m_labelStudioNetAddr = CCLabelTTF::create("www.91bit.com", "Helvetica", 16);

    m_labelStudioNetAddr->setPosition(ccp(VisibleRect::right().x +  m_labelStudioNetAddr->getContentSize().width, 
        VisibleRect::center().y - m_labelStudioNetAddr->getContentSize().height * 2.0f));

    this->addChild(m_labelStudioNetAddr);

    CCMoveTo *moveTo = CCMoveTo::create(1.5f, ccpSub(VisibleRect::center() , 
          ccp(0, m_labelStudioNetAddr->getContentSize().height * 2.0f)));
    CCEaseBackInOut *easeIn = CCEaseBackInOut::create(moveTo);
    m_labelStudioNetAddr->runAction(easeIn);
}

void SplashScene::loadLayerFadeOutAction()
{
    CCAction* action1 = CCSequence::create(
        CCDelayTime::create(1.5f),
        CCFadeOut::create(1.0f),
        CCCallFuncN::create(this, callfuncN_selector(SplashScene::splashCompleted)),
        NULL);

   m_labelStudioName->runAction(action1);

    CCAction* action2 = CCSequence::create(
         CCDelayTime::create(1.0f),
        CCFadeOut::create(1.0f),
        NULL);
    m_labelStudioNetAddr->runAction(action2);
}

void SplashScene::splashCompleted(CCNode* pSender)
{
    m_labelStudioName->stopAllActions();
    m_labelStudioNetAddr->stopAllActions();

    SceneManager::shared()->loadMainScene();

    CCLog("splashCompleted");
}

void SplashScene::loadResourcesAndFadeOut()
{
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("resources.plist");
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile("game_elements.plist");

    CCTextureCache::sharedTextureCache()->addImage("levelMap.png");
    
    loadLayerFadeOutAction();
}
