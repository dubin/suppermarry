
#include "ChallModeScene.h"

#include "utility/MenuItemImage.h"
#include "gameConfig.h"
#include "SceneManager.h"

USING_NS_CC;


ChallModeScene::ChallModeScene()
{

}

ChallModeScene::~ChallModeScene()
{

}

CCScene * ChallModeScene::scene()
{
    CCScene *scene = CCScene::create();

    ChallModeScene * layer = ChallModeScene::create();
    scene->addChild(layer);
    return scene;
}

ChallModeScene * ChallModeScene::create()
{
    ChallModeScene * layer = new ChallModeScene();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool ChallModeScene::init()
{
    this->setAnchorPoint(ccp(0,0));
    this->setPosition(0,0);

    loadMenus();

 
    return true;
}


void ChallModeScene::loadMenus()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    CCArray *menus = CCArray::create();
    MenuItemImage *backMenu = MenuItemImage::create("back_btn.png"
        , "back_btn.png"
        , this
        , menu_selector(ChallModeScene::onBackMenuToched));
    backMenu->setPosition(MENU_BACK_POS_X,MENU_BACK_POS_Y);
    menus->addObject(backMenu);

    MenuItemImage *startGameMenu = MenuItemImage::create("start_game_btn.png"
        , "start_game_btn.png"
        , this
        , menu_selector(ChallModeScene::onStartGameMenuToched));
    startGameMenu->setPosition(MENU_START_GAME_POS_X,MENU_START_GAME_POS_Y);
    menus->addObject(startGameMenu);


    CCMenu *menu = CCMenu::createWithArray(menus);
    menu->setAnchorPoint(ccp(0,0));
    menu->setPosition(0,0);
    this->addChild(menu);

    {
        CCLabelTTF *label = CCLabelTTF::create("ChallModeScene", "Helvetica", 32);

        label->setPosition(ccp( label->getContentSize().width, winSize.height - 100));
        this->addChild(label);
    }
}

void ChallModeScene::onBackMenuToched(CCObject *psender)
{
    SceneManager::shared()->loadMainScene();
}

void ChallModeScene::onStartGameMenuToched(CCObject *p)
{
    SceneManager::shared()->loadGameScene();
}