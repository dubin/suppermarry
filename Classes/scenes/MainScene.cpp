
#include "MainScene.h"

#include "utility/MenuItemImage.h"
#include "layers/HPopMenusLayer.h"

#include "SceneManager.h"
#include "gameConfig.h"



USING_NS_CC;

typedef enum
{
    kZorderMainScreenBkg = 0,
    kZorderMainScreenLabel,
    kZorderMainScreenStartButton,
    kZorderMainScreenHPOPMenu,
};

MainScene::MainScene()
{

}

MainScene::~MainScene()
{

}

CCScene * MainScene::scene()
{
    CCScene *scene = CCScene::create();

    MainScene * layer = MainScene::create();
    scene->addChild(layer);
    return scene;
}

MainScene * MainScene::create()
{
    MainScene * layer = new MainScene();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool MainScene::init()
{
    this->setAnchorPoint(ccp(0,0));
    this->setPosition(VisibleRect::leftBottom());

    CCSprite *bkg = CCSprite::createWithSpriteFrameName("gameBkg.png");
    bkg->setAnchorPoint(ccp(0,0));
    bkg->setPosition(ccp(0,0));
    this->addChild(bkg, kZorderMainScreenBkg);

    CCSprite *tileLabel = CCSprite::createWithSpriteFrameName("gameNameLabel.png");
    tileLabel->setPosition(ccp(VisibleRect::center().x,VisibleRect::center().y + 100));
    this->addChild(tileLabel, kZorderMainScreenLabel);

    loadMenus();

    loadHPOPMenus();

    CCLog("MainScene::init");

    return true;
}

void MainScene::loadMenus()
{
    CCSize winSize = VisibleRect::getVisibleRect().size;

    CCArray *menus = CCArray::create();
    MenuItemImage *taskModeMenu = MenuItemImage::create("gamePlayButton.png"
        , "gamePlayButton.png"
        , this
        , menu_selector(MainScene::onPlayGameMenuToched));
    taskModeMenu->setPosition(VisibleRect::center().x ,VisibleRect::center().y - 100);
    menus->addObject(taskModeMenu);

    CCMenu *menu = CCMenu::createWithArray(menus);
    menu->setAnchorPoint(ccp(0,0));
    menu->setPosition(VisibleRect::leftBottom());
    this->addChild(menu, kZorderMainScreenStartButton);

}

void MainScene::onPlayGameMenuToched(CCObject *sender)
{
    SceneManager::shared()->loadChooseMapScene();
}

void MainScene::loadHPOPMenus()
{
    HPopMenusLayer *hpMenu = HPopMenusLayer::create();

    hpMenu->setAnchorPoint(ccp(0,0));
    hpMenu->setPosition(VisibleRect::leftBottom().x, VisibleRect::leftBottom().y + 20);

    this->addChild(hpMenu, kZorderMainScreenHPOPMenu);
}