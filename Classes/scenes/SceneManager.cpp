#include "SceneManager.h"

#include "GameState.h"

#include "scenes/SplashScene.h"
#include "scenes/MainScene.h"
#include "scenes/ChallModeScene.h"
#include "scenes/ChooseMapScene.h"
#include "scenes/GameOverScene.h"
#include "scenes/GameScene.h"
#include "scenes/ShopScene.h"
#include "scenes/MyJewelryViewScene.h"

using namespace cocos2d;

SceneManager* SceneManager::m_instance = NULL;

SceneManager::SceneManager(void)
{
}


SceneManager::~SceneManager(void)
{
}

SceneManager * SceneManager::shared()
{
	if (m_instance == NULL)
	{
		m_instance = new SceneManager();
	}
	return m_instance;
}

CCScene * SceneManager::createSplashScene()
{
    CCScene *splash = SplashScene::scene();

    return splash;
}

void SceneManager::loadMainScene()
{
    CCTransitionFade *trans = CCTransitionFade::create(1.0f, MainScene::scene());

    CCDirector::sharedDirector()->replaceScene(trans);
}

void SceneManager::loadJewelryViewScene()
{
    CCTransitionFade *trans = CCTransitionFade::create(1.0f, MyJewelryViewScene::scene());

    CCDirector::sharedDirector()->replaceScene(trans);
}

void SceneManager::loadShopScene()
{
    CCTransitionFade *trans = CCTransitionFade::create(1.0f, ShopScene::scene());

    CCDirector::sharedDirector()->replaceScene(trans);
}

void SceneManager::loadChooseMapScene()
{
    CCTransitionFade *trans = CCTransitionFade::create(1.0f, ChooseMapScene::scene());

    CCDirector::sharedDirector()->replaceScene(trans);
}

void SceneManager::loadGameScene()
{
    CCTransitionFade *trans = CCTransitionFade::create(1.0f, GameScene::scene());

    CCDirector::sharedDirector()->replaceScene(trans);

}

void SceneManager::loadGameOverScene()
{
    GameState::shared()->stopGame();

    CCScene * scene = CCDirector::sharedDirector()->getRunningScene();
    scene->addChild(GameOverScene::create(), kZorderOverGameLayer, kTagOverGameLayer);

}











