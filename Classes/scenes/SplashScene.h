#ifndef __SPLASH_SCENE_HH
#define __SPLASH_SCENE_HH

#include "cocos2d.h"
#include "autoscale/VisibleRect.h"

USING_NS_CC;

class SplashScene : public CCLayer
{
public:
    static cocos2d::CCScene * scene();
    static SplashScene * create();

    SplashScene();
    ~SplashScene();

    virtual bool init();
    void createLabelStudioName();
    void createLabelStudioNetAddr();
    void loadLayerFadeOutAction();
    void splashCompleted(CCNode* pSender);
    void loadResourcesAndFadeOut();
private:
    CCLabelTTF *m_labelStudioName;
    CCLabelTTF *m_labelStudioNetAddr;
};



#endif