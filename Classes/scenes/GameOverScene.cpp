
#include "GameOverScene.h"

#include "utility/MenuItemImage.h"

#include "gameConfig.h"
#include "SceneManager.h"
#include "scenes/GameScene.h"

USING_NS_CC;


GameOverScene::GameOverScene()
{

}

GameOverScene::~GameOverScene()
{

}

CCScene * GameOverScene::scene()
{
    CCScene *scene = CCScene::create();

    GameOverScene * layer = GameOverScene::create();
    scene->addChild(layer);
    return scene;
}

GameOverScene * GameOverScene::create()
{
    GameOverScene * layer = new GameOverScene();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool GameOverScene::init()
{
    this->setAnchorPoint(ccp(0,0));
    this->setPosition(0,0);

    loadMenus(); 
    return true;
}


void GameOverScene::loadMenus()
{
    
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    CCArray *menus = CCArray::create();
    MenuItemImage *restartGameMenu = MenuItemImage::create("restart_game_btn.png"
        , "restart_game_btn.png"
        , this
        , menu_selector(GameOverScene::onRestartGameMenuToched));
    restartGameMenu->setPosition(MENU_PAUSE_GAME_MENUS_POS_X,
        MENU_PAUSE_GAME_MENUS_POS_Y - restartGameMenu->getContentSize().height - 50);
    menus->addObject(restartGameMenu);

    MenuItemImage *chooseLevelMenu = MenuItemImage::create("choose_level_btn.png"
        , "choose_level_btn.png"
        , this
        , menu_selector(GameOverScene::onChooseLevelMenuToched));
    chooseLevelMenu->setPosition(MENU_PAUSE_GAME_MENUS_POS_X,
        MENU_PAUSE_GAME_MENUS_POS_Y - restartGameMenu->getContentSize().height
        - restartGameMenu->getContentSize().height - 50);
    menus->addObject(chooseLevelMenu);

    CCMenu *menu = CCMenu::createWithArray(menus);
    menu->setAnchorPoint(ccp(0,0));
    menu->setPosition(0,0);
    this->addChild(menu);
}


void GameOverScene::onRestartGameMenuToched(CCObject *p)
{
    SceneManager::shared()->loadGameScene();
}


void GameOverScene::onChooseLevelMenuToched(CCObject *p)
{
    SceneManager::shared()->loadChooseMapScene();
}