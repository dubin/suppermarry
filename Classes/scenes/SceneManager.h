
#ifndef __SCENE_MANAGER_HH
#define __SCENE_MANAGER_HH

#include "cocos2d.h"
#include "typedef.h"

using namespace cocos2d;

class SceneManager
{
public:
	static SceneManager * shared();
	~SceneManager(void);

    CCScene *createSplashScene(); // 创建启动界面，进入主界面后将不能再次切换到该界面。

    void loadMainScene(); // 切换至主界面， 1， 由“启动界面”进入，
                                   // 2， 也可由 “我的徽章”界面返回
                                   // 3，“商店”界面返回
                                   // 4， “选择地图”界面返回   

    void loadJewelryViewScene(); // 切换至“我的徽章”界面， 由主界面进入，返回至“主界面”

    void loadShopScene(); // 切换至“商店”界面，由主界面进入，返回至“主界面”

    void loadChooseMapScene(); // 切换至 “选择地图” 界面， 由主界面进入。
                                   // 也可以由 “选择关卡”界面返回至此

    void loadGameScene(); // 进入 “游戏主界面” 。

    void loadGameOverScene(); // 在“游戏主界面”游戏运行结束时push进入，和 "暂停界面"一样处理

private:
	SceneManager(void);
   
    static SceneManager *m_instance;
	
};

#endif