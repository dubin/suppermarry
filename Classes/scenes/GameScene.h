#ifndef __GAME_SCENE_HH
#define __GAME_SCENE_HH


#include "cocos2d.h"
#include "utility/MenuItemImage.h"
#include "GameLayerImp.h" 

USING_NS_CC;


class GameScrollLayerImp;
class GameLayerImp;

class GameScene : public CCLayer
{
private:
    GameScene();
public:

    ~GameScene();

    static cocos2d::CCScene * scene();
    static GameScene * create();
 
    bool init();

    void loadGameLayer();
    void loadMenus();
 
    void loadGameScrollBackgroundLayer();
    void scrollingBackgroud(CCPoint delta);
     
    GameLayerImp *gameLayer() {return m_gameLayer;}
    GameScrollLayerImp *gameScrollLayer() {return m_scrollBackgroundLayer;}
    void loadHPOPMenus();
    void loadInforBar();
    void update(float fDelta);
private: 
    GameScrollLayerImp *m_scrollBackgroundLayer;
    GameLayerImp *m_gameLayer;

    CCLabelTTF *m_targetStarInfoLabel;
    CCLabelTTF *m_timeInfoLabel; 
    int m_remainTimeSecond;
    int m_remainTimeMicroSecond;
    float m_timeDelta;
};


// 用于完成游戏背景滚动
class GameScrollLayerImp : public CCLayer
{
public:

    GameScrollLayerImp();
    ~GameScrollLayerImp();

    static GameScrollLayerImp * create();
    bool init();
    void update(float fDelta);
     
    void loadBackgroud();
    void initSpeedFactors();
    void scrolling(CCPoint delta);
private:
    CCSpriteBatchNode *m_spriteBatchNode;
    int m_numberSprites;

    std::vector<float> m_speedFactors;

    float m_scrollScale;
    CCSize winSize;
     
};

#endif
