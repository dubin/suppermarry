#ifndef __CHALL_MODE_SCENE_HH
#define __CHALL_MODE_SCENE_HH


#include "cocos2d.h"

USING_NS_CC;

class ChallModeScene : public CCLayer
{
public:

    ChallModeScene();
    ~ChallModeScene();

    static cocos2d::CCScene * scene();
    static ChallModeScene * create();
    bool init();

    void loadMenus();
    void onBackMenuToched(CCObject *psender);
    void onStartGameMenuToched(CCObject *p);
};



#endif
