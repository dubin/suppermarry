
#include "ChooseMapScene.h"

#include "utility/MenuItemImage.h"

#include "gameConfig.h"
#include "SceneManager.h"
#include "autoscale/VisibleRect.h"
#include "layers/HPopMenusLayer.h"

USING_NS_CC;

typedef enum
{
    kZorderMapScreenBkg = 0,
    kZorderMapScreenShopButton = 0,

    kZorderMapScreentTop,
};

ChooseMapScene::ChooseMapScene()
{

}

ChooseMapScene::~ChooseMapScene()
{

}

CCScene * ChooseMapScene::scene()
{
    CCScene *scene = CCScene::create();

    ChooseMapScene * layer = ChooseMapScene::create();
    scene->addChild(layer);
    return scene;
}

ChooseMapScene * ChooseMapScene::create()
{
    ChooseMapScene * layer = new ChooseMapScene();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool ChooseMapScene::init()
{
    this->setAnchorPoint(ccp(0,0));
    this->setPosition(0,0);

    loadMapBkg();
    loadMenus();
    loadHPOPMenus();

    loadGoldIcon();
 
    return true;
}

void ChooseMapScene::loadMenus()
{
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    CCArray *menus = CCArray::create();
    MenuItemImage *shopIcon = MenuItemImage::create("shopIcon.png"
        , "shopIcon.png"
        , this
        , menu_selector(ChooseMapScene::onShopMenuToched));
    shopIcon->setPosition(VisibleRect::leftTop().x ,VisibleRect::leftTop().y - shopIcon->getContentSize().height);
    shopIcon->setAnchorPoint(ccp(0,0));
    menus->addObject(shopIcon);


    {// for test
        MenuItemImage *play = MenuItemImage::create("gamePlayButton.png"
            , "gamePlayButton.png"
            , this
            , menu_selector(ChooseMapScene::onPlayBtnTouched));
        play->setPosition(VisibleRect::center());
        play->setAnchorPoint(ccp(0,0));
        menus->addObject(play);
    }
 
    CCMenu *menu = CCMenu::createWithArray(menus);
    menu->setAnchorPoint(ccp(0,0));
    menu->setPosition(0,0);
    this->addChild(menu, kZorderMapScreenShopButton);
}

void ChooseMapScene::loadGoldIcon()
{
    CCSprite *goldIcon = CCSprite::createWithSpriteFrameName("goldIcon.png");
    goldIcon->setPosition(ccp(VisibleRect::leftTop().x ,VisibleRect::leftTop().y
        - goldIcon->getContentSize().height -  goldIcon->getContentSize().height - 20));
    goldIcon->setAnchorPoint(ccp(0,0));

    this->addChild(goldIcon, kZorderMapScreenShopButton);
}

void ChooseMapScene::onShopMenuToched(CCObject *p)
{
    SceneManager::shared()->loadMainScene();
}

void ChooseMapScene::onPlayBtnTouched(CCObject *p)
{
    SceneManager::shared()->loadGameScene();
}

void ChooseMapScene::loadMapBkg()
{ 
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage("levelMap.png");

    CCSprite *bkg = CCSprite::createWithTexture(texture);

    bkg->setAnchorPoint(ccp(0,0));
    bkg->setPosition(ccp(0,0));
    this->addChild(bkg, kZorderMapScreenBkg);
}

void ChooseMapScene::loadHPOPMenus()
{
    HPopMenusLayer *hpMenu = HPopMenusLayer::create();

    hpMenu->setAnchorPoint(ccp(0,0));
    hpMenu->setPosition(VisibleRect::leftBottom().x, VisibleRect::leftBottom().y + 20);

    this->addChild(hpMenu, kZorderMapScreentTop);
}

