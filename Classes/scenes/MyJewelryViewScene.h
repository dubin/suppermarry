#ifndef __MY_JEWELRY_VIEW_SCENE_HH
#define __MY_JEWELRY_VIEW_SCENE_HH


#include "cocos2d.h"

USING_NS_CC;

class MyJewelryViewScene : public CCLayer
{
public:

    MyJewelryViewScene();
    ~MyJewelryViewScene();

    static cocos2d::CCScene * scene();
    static MyJewelryViewScene * create();
    bool init();

private:
    void loadGoldLabel(); 
    void loadListView();
};



#endif
