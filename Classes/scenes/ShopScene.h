#ifndef __SHOP_SCENE_HH
#define __SHOP_SCENE_HH


#include "cocos2d.h"

USING_NS_CC;

class ShopScene : public CCLayer
{
public:

    ShopScene();
    ~ShopScene();

    static cocos2d::CCScene * scene();
    static ShopScene * create();
    bool init();

    void loadMenus(); 
    void onBackMenuToched(CCObject *sender);
};



#endif
