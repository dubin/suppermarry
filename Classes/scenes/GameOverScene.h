#ifndef __GAME_OVER_SCENE_HH
#define __GAME_OVER_SCENE_HH


#include "cocos2d.h"

USING_NS_CC;

class GameOverScene : public CCLayer
{
public:

    GameOverScene();
    ~GameOverScene();

    static cocos2d::CCScene * scene();
    static GameOverScene * create();
    bool init();

    void loadMenus();
    void onRestartGameMenuToched(CCObject *p);
    void onChooseLevelMenuToched(CCObject *p);
};



#endif
