
#include "MyJewelryViewScene.h"

#include "utility/MenuItemImage.h"

#include "gameConfig.h"
#include "SceneManager.h"
#include "autoscale/VisibleRect.h"
#include "layers/MyJewelryListViewLayer.h"

USING_NS_CC;

MyJewelryViewScene::MyJewelryViewScene()
{

}

MyJewelryViewScene::~MyJewelryViewScene()
{

}

CCScene * MyJewelryViewScene::scene()
{
    CCScene *scene = CCScene::create();

    MyJewelryViewScene * layer = MyJewelryViewScene::create();
    scene->addChild(layer);
    return scene;
}

MyJewelryViewScene * MyJewelryViewScene::create()
{
    MyJewelryViewScene * layer = new MyJewelryViewScene();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool MyJewelryViewScene::init()
{
    this->setAnchorPoint(ccp(0,0));
    this->setPosition(0,0);

    loadGoldLabel();
    loadListView();

    return true;
}


void MyJewelryViewScene::loadGoldLabel()
{
    CCSprite *sprite = CCSprite::createWithSpriteFrameName("goldIcon.png");
    sprite->setAnchorPoint(ccp(0,0));
    sprite->setPosition(ccpSub(VisibleRect::leftTop(), ccp(0, sprite->getContentSize().height - 50)));
    this->addChild(sprite);

}

void MyJewelryViewScene::loadListView()
{
    MyJewelryListViewLayer *listView = MyJewelryListViewLayer::create();

    listView->setAnchorPoint(ccp(0,0));
    listView->setPosition(10,200);
    this->addChild(listView);
}
