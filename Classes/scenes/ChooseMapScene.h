#ifndef __CHOOSE_MAP_SCENE_HH
#define __CHOOSE_MAP_SCENE_HH


#include "cocos2d.h"

USING_NS_CC;

class ChooseMapScene : public CCLayer
{
public:

    ChooseMapScene();
    ~ChooseMapScene();

    static cocos2d::CCScene * scene();
    static ChooseMapScene * create();
    bool init();

    void loadMenus();
    void onShopMenuToched(CCObject *p);
    void onPlayBtnTouched(CCObject *p);

    void loadMapBkg();
    void loadGoldIcon();
    void loadHPOPMenus();
   
};



#endif
