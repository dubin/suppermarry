#include "GameLayerImp.h"

#include "database/GameDataSource.h"
#include "AI/HeroAI.h"
#include "autoscale/VisibleRect.h"


#include <math.h>

typedef enum
{
    KZorderGameObject = 2,
    KZorderGameTop,
} GOZOrder;

//////////////////////////////////////////////////////////////////////////
GameLayerImp::GameLayerImp()
{
}

GameLayerImp::~GameLayerImp()
{
}

GameLayerImp * GameLayerImp::create()
{
    GameLayerImp * layer = new GameLayerImp();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool GameLayerImp::init()
{
    this->setTouchMode(kCCTouchesOneByOne);
    this->setTouchEnabled(true); 
     
    winSize = VisibleRect::getVisibleRect().size;

    m_map.setTiledMapTiledSize(GameDataSource::shared()->getTiledWidth(), 
                               GameDataSource::shared()->getTiledHeight());

    initGameObjectCache();
    initHero();
    initLevel();
 
    this->scheduleUpdate();

    m_heroAi = HeroAI::createWithTiledMap(m_hero, &m_map, this);
    this->addChild(m_heroAi);
    
    schedule(schedule_selector(GameLayerImp::tick), GameDataSource::shared()->getHeroMoveDuration());

    return true;
}

void GameLayerImp::initHero()
{
    LevelData *levelData = GameDataSource::shared()->getLevelData();
    if (levelData == NULL)
    {
        CCLog("Get level Data Failed.");
        return;
    }
     
    m_hero = HeroFactory::createHero(HERO_TYPE_1);

    CCPoint pixelPoint = 
        m_map.mapTileCoordToPixel(ccp(levelData->m_heroStartCol, levelData->m_heroStartRow));

    m_hero->setPosition(ccpAdd(pixelPoint, 
        ccp(GameDataSource::shared()->getTiledWidth()/2  , m_hero->getContentSize().height/2 * m_hero->getUserMultiScale())));
 
    // added by db, 2013.2.7
    m_heroLastPos = pixelPoint;

    this->addChild(m_hero, KZorderGameObject);
}


void GameLayerImp::initGameObjectCache()
{
    m_gameObjectCache = GameObjectCache::create();
    this->addChild(m_gameObjectCache, KZorderGameObject + 1);
}

void GameLayerImp::registerWithTouchDispatcher()
{
    CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, false);
}

void GameLayerImp::update(float fDelta)
{

}

void GameLayerImp::tick(float fDelta)
{
    if (GameState::shared()->state() != kGameStateRunning)
    {
        return;
    }
    
    // deleted by db 2013.5.7
    //int step = GameDataSource::sharedDataSource()->getHeroMoveStep();
    int rightSide = GameDataSource::shared()->getRightSideMaxInPixel();
    
    CCPoint pos = m_hero->getPosition();

    // added by db 2013.5.7
    float step = pos.x - m_heroLastPos.x;
    m_heroLastPos = pos;
    // ended

    if (step == 0)
        return;

    GameScene *gameScene = (GameScene*)this->getParent();
    
    if (pos.x >= rightSide - this->getPositionX() || (pos.x < 100 - this->getPositionX() && step < 0 ))
    {
        this->setPositionX(this->getPositionX() - step);
        gameScene->scrollingBackgroud(ccp(-step,0));
    }
}

void GameLayerImp::onExit()
{
    m_heroAi->unscheduleAllSelectors();
    m_heroAi->removeFromParentAndCleanup(true);

    CCLayer::onExit();
}

bool GameLayerImp::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
    CCPoint pos = getLocationToLayer(pTouch->getLocation());
    
    int t = rand() % (BLOCK_TYPE_MAX - 1) + 1;
    addObjectAt(pos, t);

    return true;
}

void GameLayerImp::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{ 

    CCPoint pos = getLocationToLayer(pTouch->getLocation());

    int t = rand() % (BLOCK_TYPE_MAX - 1) + 1;
    addObjectAt(pos, t);
}

void GameLayerImp::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{ 

}

void GameLayerImp::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{ 
}

void GameLayerImp::initLevel()
{
    LevelData *levelData = GameDataSource::shared()->getLevelData();

    if (levelData == NULL)
    {
        CCLog("Get Level Data Failed.");
        return;
    }

    std::vector<LevelMetaData> *metaData = levelData->getData();
    if (metaData == NULL)
    {
        return;
    }

    int num = metaData->size();
    int index = 0;
    for (index = 0; index < num; index ++)
    {
        LevelMetaData md = metaData->at(index);
        addObjectAtImpl(md.col, md.row, md.type);
    }

    GameDataSource::shared()->initTouchedStarNum();
}

// added by db 2013.5.8
bool  GameLayerImp::addObjectAtImpl(int col, int row, int type)
{
    
    bool flag = m_map.insertTileByTiledCoord(ccp(col,row), type);

    if (flag == true)
    {
        m_gameObjectCache->addObjectAt(m_map.mapTileCoordToPixel(ccp(col,row)), type);
        return true;
    }

    return false;
}

void GameLayerImp::addObjectAt(CCPoint loc, int type)
{
    CCPoint tileCoord = m_map.PixelCoordToMapTiled(loc);

    addObjectAt((int)tileCoord.x, (int)tileCoord.y, type);
}

void GameLayerImp::addObjectAt(int col, int row, int type)
{
    if (GameState::shared()->state() != kGameStateRunning)
    {
        return;
    }

    if (GameDataSource::shared()->getTotalGold() < 1)
    {
        CCLog("Not enough Money.");
        return;
    }

    CCPoint pos = m_hero->getPosition();
    CCPoint tiledCoord = m_map.PixelCoordToMapTiled(pos);

    int currentType = m_map.typeByTiledCoord(ccp(col, row));
    int down = m_map.typeByTiledCoord(ccp(col, row-1));
    int up = m_map.typeByTiledCoord(ccp(col, row+1));
    int left = m_map.typeByTiledCoord(ccp(col-1, row));
    int right = m_map.typeByTiledCoord(ccp(col+1, row));

    if (row <= (int)GameDataSource::shared()->m_limitedRowMin ||
         row >= (int)GameDataSource::shared()->m_limitedRowMax)
    {
        return;
    }

    if (!m_map.isEmpty(currentType))
    {
        return;
    }

    if (m_map.isEmpty(down) && m_map.isEmpty(up) 
       && m_map.isEmpty(left) && m_map.isEmpty(right))
    {
        return;
    }

    if (m_map.isProps(down))
    {
        return;
    }

    if (addObjectAtImpl(col, row, type))
    {
        addMagicObj(col, row + 1, type);
    }
}

CCPoint GameLayerImp::getLocationToLayer(CCPoint screenLoc)
{
    return ccpSub(screenLoc, this->getPosition());
}

void GameLayerImp::rmObjectAtImpl(int col, int row)
{
    CCPoint pos = m_map.mapTileCoordToPixel(ccp(col, row));
    m_gameObjectCache->rmObjectAt(
        ccpAdd(pos, 
        ccp(GameDataSource::shared()->getTiledWidth() / 2, GameDataSource::shared()->getTiledHeight() / 2)
        ));
    
    int type = m_map.typeByTiledCoord(ccp(col,row));

    if (m_map.isProps(type))
    {
        runPropsDieAction(pos, type);
    }
    else
    {
        runBlockDieAction(pos, type);
    }
}

void GameLayerImp::rmObjectAt(int col, int row)
{
    rmObjectAtImpl(col, row);
    m_map.clearTiledTypeByTiledCoord(ccp(col, row));
}

void GameLayerImp::runPropsDieAction(CCPoint pos, int type)
{
    CCSprite *sprite = GameObjectFactory::createObject(type);
    sprite->setPosition(pos);
    this->addChild(sprite, KZorderGameTop);

    CCScaleTo *scale = CCScaleTo::create(0.6f, 0.5f);
    CCMoveBy *move = CCMoveBy::create(0.6f, ccp(0, 10.0f));

    CCFadeOut *fo = CCFadeOut::create(0.8f);
     
    CCSequence *seq = CCSequence::create(fo,
        CCCallFuncND::create(this, callfuncND_selector(GameLayerImp::onPropEatActionDone), (void*)sprite),
         NULL);
    sprite->runAction(move);
    sprite->runAction(scale);
    sprite->runAction(seq);
}

void GameLayerImp::onPropEatActionDone(CCNode *target, void *data)
{
    CCSprite *sprite = (CCSprite *)data;
    
    sprite->stopAllActions();

    sprite->removeFromParentAndCleanup(true);
}

void GameLayerImp::runBlockDieAction(CCPoint pos, int type)
{
    CCSprite *sprite = GameObjectFactory::createObject(type);
    sprite->setAnchorPoint(ccp(0.5f,0.5f));

    sprite->setPosition(ccpAdd(pos, 
        ccp(GameDataSource::shared()->getTiledWidth() / 2, sprite->getContentSize().height / 2)));

    this->addChild(sprite, KZorderGameTop);

    CCScaleTo *scale1 = CCScaleTo::create(0.2f, 1.2f);
    CCScaleTo *scale2 = CCScaleTo::create(0.6f, 0.1f);

    CCFadeOut *fo = CCFadeOut::create(0.8f);

    CCSequence *seq = CCSequence::create(scale1,scale2,
        CCCallFuncND::create(this, callfuncND_selector(GameLayerImp::onBlockDieActionDone), (void*)sprite),
        NULL);
    sprite->runAction(fo);
    sprite->runAction(seq);
}

void GameLayerImp::onBlockDieActionDone(CCNode *target, void *data)
{
    CCSprite *sprite = (CCSprite *)data;

    sprite->stopAllActions();

    sprite->removeFromParentAndCleanup(true);
}


void GameLayerImp::addMagicObj(int col, int row, int type)
{
    if (type != BLOCK_TYPE_2)
    {
        return;
    }

    int currentType = m_map.typeByTiledCoord(ccp( col, row));
    if (m_map.isBlock(currentType) || m_map.isProps(currentType))
    {
        return;
    }

    CCPoint pos = m_map.mapTileCoordToPixel(ccp(col, row));
    
    Bomb *bomb = Bomb::createAndInit(BOMB_TYPE_1, this);
    this->addChild(bomb, KZorderGameTop);

    pos = ccpAdd(ccp(GameDataSource::shared()->getTiledWidth() / 2, 
        bomb->getContentSize().height / 2),pos);

    bomb->addObjectAt(pos);
}
 
void GameLayerImp::rmMagicObjectAtImpl(CCPoint pos)
{ 
    CCPoint tiledCoord = m_map.PixelCoordToMapTiled(pos);
    rmObjectAt(tiledCoord.x , tiledCoord.y - 1);
}
