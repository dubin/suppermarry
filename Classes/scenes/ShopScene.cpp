
#include "ShopScene.h"

#include "utility/MenuItemImage.h"

#include "gameConfig.h"
#include "SceneManager.h"

USING_NS_CC;


ShopScene::ShopScene()
{

}

ShopScene::~ShopScene()
{

}

CCScene * ShopScene::scene()
{
    CCScene *scene = CCScene::create();

    ShopScene * layer = ShopScene::create();
    scene->addChild(layer);
    return scene;
}

ShopScene * ShopScene::create()
{
    ShopScene * layer = new ShopScene();
    if (layer && layer->init())
        layer->autorelease();
    else
        CC_SAFE_DELETE(layer);
    return layer;
}

bool ShopScene::init()
{
    this->setAnchorPoint(ccp(0,0));
    this->setPosition(0,0);

    loadMenus(); 

    return true;
}


void ShopScene::loadMenus()
{ 
    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

    CCArray *menus = CCArray::create();
    MenuItemImage *backMenu = MenuItemImage::create("back_btn.png"
        , "back_btn.png"
        , this
        , menu_selector(ShopScene::onBackMenuToched));
    backMenu->setPosition(MENU_BACK_POS_X,MENU_BACK_POS_Y);
    menus->addObject(backMenu);

    CCMenu *menu = CCMenu::createWithArray(menus);
    menu->setAnchorPoint(ccp(0,0));
    menu->setPosition(0,0);
    this->addChild(menu);


    {
        CCLabelTTF *label = CCLabelTTF::create("ShopScene", "Helvetica", 32);

        label->setPosition(ccp( label->getContentSize().width, winSize.height - 100));
        this->addChild(label);
    }
}

void ShopScene::onBackMenuToched(CCObject *sender)
{
    SceneManager::shared()->loadMainScene();
}