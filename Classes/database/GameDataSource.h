
#ifndef __GAME_DATA_SRC_HH
#define __GAME_DATA_SRC_HH

#include <string>
#include <vector>
#include "GameLevelDataSource.h"

using namespace std;

class GameDataSource 
{
private:
    GameDataSource();
    static GameDataSource *m_instance;

public:
    ~GameDataSource();

    static GameDataSource *shared()
    {
        if (m_instance == NULL)
        {
            m_instance = new GameDataSource();
            m_instance->loadLevelData();
        }
        return m_instance;
    }

public: 
    unsigned int getCurrentLevelIndex() {return m_currentLevelIndex;}
    unsigned int getTotalLevelCount() 
    {
        if (m_gameLevelDataSource != NULL)
        {
            return m_gameLevelDataSource->getTotalLevelCount();
        }

        return 0;
    }

    void setCurrentLevelIndex(int index) {m_currentLevelIndex = index;}
    LevelData * getLevelData();

    unsigned int getTouchedStarNum() {return m_touchedStarNum;}
    void addTouchedStarNum(int num) 
    {
        LevelData *ld= getLevelData();
        if (m_touchedStarNum >= ld->m_targetTouchedStarNum)
            return;
        m_touchedStarNum += num;
    }
    void initTouchedStarNum() {m_touchedStarNum = 0;}
private:
    unsigned int m_touchedStarNum;
    unsigned int m_currentLevelIndex;
    void loadLevelData();
    GameLevelDataSource *m_gameLevelDataSource;
    
// ----
public:
    float getHeroMoveDuration(){return m_heroMoveDuration;}
    void setHeroMoveDuration(float dur){m_heroMoveDuration = dur;}

    int getHeroMoveStep(){return m_heroMoveStep;}
    void setHeroMoveStep(float step){m_heroMoveStep = step;}

    int getRightSideMaxInPixel(){return m_rightSideMaxCol;}
    void setRightSideMaxCol(int max){m_rightSideMaxCol = max;}

    int getBottomSideInPixel() {return m_bottomSideMaxPixel;}
    void setBottomSideInPixel(int max){m_bottomSideMaxPixel = max;}

    float getGamebackgroudScrollingSpreed(){return m_gamebackgroudScrollingSpreed;}
    void  setGamebackgroudScrollingSpreed(float speed){ m_gamebackgroudScrollingSpreed = speed;}
private:
    float m_heroMoveDuration;
    int m_heroMoveStep;
    int m_rightSideMaxCol;
    int m_bottomSideMaxPixel;
    float m_gamebackgroudScrollingSpreed;
// ---
public:
    int getTiledWidth(){return m_tiledWidth;}
    int getTiledHeight(){return m_tiledHeight;}
 
    void setTiledWidth(int w){  m_tiledWidth = w;}
    void setTiledHeight(int h){  m_tiledHeight = h;}

private:
    int m_tiledWidth;
    int m_tiledHeight;

// --
public:
   unsigned int getBatchNodeMaxNum() {return m_batchNodeMaxNumForBP;}
    void setBatchNodeMaxNum(int num) {m_batchNodeMaxNumForBP = num;}

private:
   unsigned int m_batchNodeMaxNumForBP;

/// ----
public:
    void setTotalGold(int num) {m_totalGold = num;}
    unsigned int getTotalGold() {return m_totalGold;}
    void subTotalGold(unsigned int n) {if (m_totalGold > 0) m_totalGold -= n; }
private:
    unsigned int m_totalGold;


///-

public:
    unsigned int m_limitedRowMax;
    unsigned int m_limitedRowMin;
private:
    void calcLimitedRow();
    void setBottomPadding(float pad){m_bottomPadding = pad;}
    void setTopPadding(float pad){m_topPadding = pad;}

    float m_bottomPadding;
    float m_topPadding;
};







#endif