
#include "GameLevelDataSource.h"

#include "typedef.h"

LevelMetaData::LevelMetaData()
{
    this->row = 0;
    this->col = 0;
    this->type = BP_TYPE_NULL;
}

LevelMetaData::LevelMetaData(int col, int row, int type)
{
    this->row = row;
    this->col = col;
    this->type = type;
}



LevelData::LevelData()
{
    m_levelIndex = 0;
    m_levelMetaData.clear();
}
 
LevelData::~LevelData()
{
    m_levelMetaData.clear();
}

std::vector<LevelMetaData> * LevelData::getData()
{
    return &m_levelMetaData;
}

void LevelData::addMetaData( int col, int row, int type )
{
    m_levelMetaData.push_back(LevelMetaData(col, row, type));
}

void LevelData::init( unsigned int index, unsigned int second, int distance )
{
    m_levelIndex = index;
}


GameLevelDataSource::GameLevelDataSource()
{
    m_dataSource.clear();
}

GameLevelDataSource::~GameLevelDataSource()
{
    m_dataSource.clear();
}

void GameLevelDataSource::loadLevelsData()
{

    {// Level 1
        LevelData data;

        data.m_targetTouchedStarNum = 25;

        data.init(1, 30, 1024);
        data.m_heroStartCol = 3;
        data.m_heroStartRow = 6;

        for (int index = 0; index < 12; index ++)
        {
            data.addMetaData(index , data.m_heroStartRow - 1, BLOCK_TYPE_1);
        }

        data.addMetaData(10 ,6, PROPS_TYPE_1);
        data.addMetaData(13 ,8, PROPS_TYPE_1);
        data.addMetaData(15 ,5, PROPS_TYPE_1);
        data.addMetaData(16 ,7, PROPS_TYPE_1);
        data.addMetaData(18 ,5, PROPS_TYPE_1);
        data.addMetaData(20 ,4, PROPS_TYPE_1);
        data.addMetaData(21 ,8, PROPS_TYPE_1);

        data.addMetaData(30 ,6, PROPS_TYPE_1);
        data.addMetaData(33 ,8, PROPS_TYPE_1);
        data.addMetaData(35 ,5, PROPS_TYPE_1);
        data.addMetaData(36 ,7, PROPS_TYPE_1);
        data.addMetaData(38 ,5, PROPS_TYPE_1);
        data.addMetaData(30 ,4, PROPS_TYPE_1);
        data.addMetaData(31 ,8, PROPS_TYPE_1);

        addLevelData(data);
    }

    m_totalLevelCount = m_dataSource.size();
}

LevelData * GameLevelDataSource::getLevelDataByIndex( unsigned int index)
{
    int num = m_dataSource.size();
    int j = 0;
    LevelData *data = NULL;

    for (j = 0; j < num; j++)
    {
        data = &m_dataSource.at(j);

        if (data->m_levelIndex == index)
            return data;
    }

    return NULL;
}

void GameLevelDataSource::addLevelData( LevelData &data )
{
    m_dataSource.push_back(data);
}
