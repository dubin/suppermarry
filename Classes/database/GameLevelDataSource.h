#ifndef __GAME_LEVEL_DATA_SOURCE_HH
#define __GAME_LEVEL_DATA_SOURCE_HH

#include <string>
#include <vector>

using namespace std;

class LevelMetaData
{
public:
    LevelMetaData();
    LevelMetaData(int row, int col, int type);

    int row;
    int col;
    int type;
};

class LevelData
{
public:
    LevelData(); 
    ~LevelData();

    void init(unsigned int index, unsigned int second, int distance);
    unsigned int m_levelIndex; // 本关序号
    unsigned int m_targetTouchedStarNum; // 本关要收集到的星星道具数。

    int m_heroStartCol;
    int m_heroStartRow;

    std::vector<LevelMetaData> *getData();
    
    void addMetaData(int row, int col, int type);

private:
    std::vector<LevelMetaData> m_levelMetaData;
};

class GameLevelDataSource
{
public:
    GameLevelDataSource();
    ~GameLevelDataSource();

    void loadLevelsData();

    unsigned int getTotalLevelCount() {return m_totalLevelCount;}

    LevelData *getLevelDataByIndex(unsigned int index);

private:
    void addLevelData(LevelData &data);

private:
    unsigned int m_totalLevelCount; 
    std::vector<LevelData> m_dataSource;
};




#endif