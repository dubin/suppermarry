
#include "GameDataSource.h"
#include "gameConfig.h"

#include "autoscale/VisibleRect.h"

GameDataSource *GameDataSource::m_instance = NULL;


//////////////////////////////////////////////////////////////////////////
GameDataSource::GameDataSource()
{
    setHeroMoveDuration(0.01f);
    setHeroMoveStep(1);
    setRightSideMaxCol(200);
    setBottomSideInPixel(100);

    setTiledHeight(60);
    setTiledWidth(60);

    setGamebackgroudScrollingSpreed(-100.0f);

    setBatchNodeMaxNum(1024);

    setTotalGold(999999);

    setCurrentLevelIndex(1);
    m_gameLevelDataSource = NULL;
    initTouchedStarNum();

    setBottomPadding(100.0f);
    setTopPadding(100.0f);
    calcLimitedRow();
}

GameDataSource::~GameDataSource()
{
    if (m_gameLevelDataSource != NULL)
    {
        delete m_gameLevelDataSource;
    }
}

void GameDataSource::loadLevelData()
{
    m_gameLevelDataSource = new GameLevelDataSource();

    if (m_gameLevelDataSource != NULL)
    {
        m_gameLevelDataSource->loadLevelsData();
    }
}

LevelData * GameDataSource::getLevelData()
{
    if (m_gameLevelDataSource == NULL)
        return NULL;

    return m_gameLevelDataSource->getLevelDataByIndex(getCurrentLevelIndex());
}

void GameDataSource::calcLimitedRow()
{
    m_limitedRowMin = (m_bottomPadding + GameDataSource::getTiledHeight() ) 
                 / GameDataSource::getTiledHeight();
    m_limitedRowMax = 
        (VisibleRect::getVisibleRect().size.height - m_topPadding) 
              / GameDataSource::getTiledHeight();

}

