#ifndef __GAME_STATE_HH
#define __GAME_STATE_HH

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

typedef enum
{
    kGameStateUnknown = 1,
    kGameStatePause,
    kGameStateRunning,
    kGameStateStoped,
}GAME_STATE_E;

class GameState
{
private:
    GameState()
    {}

    static GameState *_instance;
public:
    ~GameState()
    {
    }

    static GameState *shared()
    {
        if (_instance == NULL)
            _instance = new GameState();

        return _instance;
    }

    void pauseGame()
    {
        m_gameState = kGameStatePause;
    }

    void resumeGame()
    {
        m_gameState = kGameStateRunning;
    }

    void stopGame()
    {
        m_gameState = kGameStateStoped;
    }

    void startGame()
    {
        m_gameState = kGameStateRunning;
    }

    GAME_STATE_E state()
    {
        return m_gameState;
    }

private:
    GAME_STATE_E m_gameState;

};

#endif